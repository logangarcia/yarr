<!--YARR (Yet Another RSS Reader)
    Copyright (C) 2017-2018  Logan Garcia

    This file is part of YARR.

    YARR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    YARR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARR.  If not, see <http://www.gnu.org/licenses/>.-->

<resources>
    <string name="app_name" translatable="false">YARR</string>

    <!-- strings related to clickable behavior -->
    <string name="action_search">Search</string>
    <string name="action_edit">Edit</string>
    <string name="action_rename">Rename</string>
    <string name="action_delete">Delete</string>
    <string name="action_mark_all_read">Mark all as read</string>
    <string name="action_mark_all_unread">Mark all as unread</string>
    <string name="action_refresh">Refresh</string>
    <string name="action_settings">Settings</string>
    <string name="action_sign_out">Sign out</string>
    <string name="action_drawer_open">Open navigation drawer</string>
    <string name="action_drawer_close">Close navigation drawer</string>
    <string name="action_view_unread">Unread</string>
    <string name="action_view_all">All</string>
    <string name="action_view_starred">Starred</string>
    <string name="action_mark_unread">Mark as unread</string>
    <string name="action_mark_read">Mark as read</string>
    <string name="action_add_star">Add star</string>
    <string name="action_remove_star">Remove star</string>
    <string name="action_share">Share</string>
    <string name="action_open_browser">Open in browser</string>
    <string name="action_done">Done</string>
    <string name="action_sign_in">Sign in</string>
    <string name="action_sync_wifi">Only on Wi-Fi</string>
    <string name="action_sync_charge">Only while charging</string>
    <string name="action_dark_theme">Dark theme</string>
    <string name="action_notifications">Notifications</string>
    <string name="action_vibrate">Vibrate</string>
    <string name="action_import_opml">Import feeds from OPML</string>
    <string name="action_export_opml">Export feeds to OPML</string>
    <string name="action_undo">Undo</string>
    <string name="action_download">Download</string>

    <!-- strings related to non-clickable dialogue messages -->
    <string name="dialogue_all">All</string>
    <string name="dialogue_unread">Unread</string>
    <string name="dialogue_starred">Starred</string>
    <string name="dialogue_subscriptions">Feeds</string>
    <string name="dialogue_no_entries">There are no entries here.</string>
    <string name="dialogue_share">Share via</string>
    <string name="dialogue_add_subscription">Add new feed</string>
    <string name="dialogue_tags_help">Comma separated</string>
    <string name="dialogue_field_required">* Required fields</string>
    <string name="dialogue_sign_in">Sign in</string>
    <string name="dialogue_rename_tag">Rename tag</string>
    <string name="dialogue_edit_subscription">Edit feed</string>
    <string name="dialogue_general">General</string>
    <string name="dialogue_notification">Notification</string>
    <string name="dialogue_sync">Data usage</string>
    <string name="dialogue_settings">Settings</string>
    <string name="dialogue_sound_none">None</string>
    <string name="dialogue_import_export">Import and export</string>
    <string name="dialogue_search">Search entries</string>

    <!--strings related to displaying error messages -->
    <string name="error_field_required">This field is required</string>
    <string name="error_incorrect_email_password">Email or password is incorrect</string>
    <string name="error_no_network">Unable to connect to network</string>
    <string name="error_add_subscription">Unable to add feed</string>

    <!-- strings related to editable text fields -->
    <string name="field_url_required">URL *</string>
    <string name="field_name">Name</string>
    <string name="field_tags">Tags</string>
    <string name="field_email">Email</string>
    <string name="field_password">Password</string>
    <string name="field_name_required">Name *</string>

    <!-- strings related to popup boxes and alerts -->
    <string name="popup_delete_positive">Delete</string>
    <string name="popup_delete_negative">Cancel</string>
    <string name="popup_sync_positive">OK</string>
    <string name="popup_sync_negative">Cancel</string>
    <string name="popup_delete_subscription_title">Delete feed?</string>
    <string name="popup_delete_subscription_message">All entries of this feed (including starred entries) will be deleted.</string>
    <string name="popup_delete_tag_title">Delete tag?</string>
    <string name="popup_delete_tag_message">Feeds will no longer be able to be found under this tag.</string>
    <string name="popup_download_message">Download file?</string>
    <string name="popup_download_positive">Download</string>
    <string name="popup_download_negative">Cancel</string>
    <string name="popup_sync_frequency">Sync frequency</string>
    <string name="popup_sound">Sound</string>
    <string name="popup_sync_never">Never</string>
    <string name="popup_sync_15_minutes">Every 15 minutes</string>
    <string name="popup_sync_30_minutes">Every 30 minutes</string>
    <string name="popup_sync_1_hour">Every hour</string>
    <string name="popup_sync_3_hours">Every 3 hours</string>
    <string name="popup_sync_6_hours">Every 6 hours</string>
    <string name="popup_sync_12_hours">Every 12 hours</string>
    <string name="popup_sync_1_day">Every day</string>
    <string name="popup_notification_unread_entry">unread entry</string>
    <string name="popup_notification_unread_entries">unread entries</string>
    <string name="popup_sign_out_title">Sign out?</string>
    <string name="popup_sign_out_message">You will have to sign back in to use YARR.</string>
    <string name="popup_sign_out_positive">Sign out</string>
    <string name="popup_sign_out_negative">Cancel</string>
    <string name="popup_choose_file_title">Choose a file</string>
    <string name="popup_choose_file_positive">OK</string>
    <string name="popup_choose_file_negative">Cancel</string>
    <string name="popup_export_message">Export feeds?</string>
    <string name="popup_export_positive">Export</string>
    <string name="popup_export_negative">Cancel</string>
    <string name="popup_export_location">Exported feeds to</string>
    <string name="popup_marked_as_read">Marked as read</string>
    <string name="popup_marked_as_unread">Marked as unread</string>
    <string name="popup_import_success">Successfully imported the feeds</string>
    <string name="popup_import_failure">Failed to import the feeds</string>
    <string name="popup_notification_running">YARR is running</string>
</resources>
