/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.activity.FeedActivity;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Service that updates the local database from the server.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class UpdateService extends Service {
    // define broadcast identifier
    public static final String ACTION_BROADCAST = "me.cgarcia.yarr.BROADCAST";

    // define a final handler
    private final Handler mHandler = new Handler();

    // declare local variables
    private Runnable mRunnable;
    private boolean mIsUpdating;

    @Override
    public void onCreate() {
        super.onCreate();

        // set the boolean value
        mIsUpdating = false;

        // set the runnable
        setRunnable();

        // declare a notification intent
        Intent notificationIntent = new Intent(this, FeedActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        // build the notification
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(this.getResources().getString(R.string.popup_notification_running))
                .setPriority(Notification.PRIORITY_MIN)
                .setContentIntent(pendingIntent).build();

        // set the notification as foreground to
        // prevent the service from being killed
        startForeground(2, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // start handler from sync time
        startHandler();

        //update the database
        update();

        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Calls an AsyncTask to update the database from the server.
     */
    private void update() {
        if (DefaultApplication.isConnected(this) && !mIsUpdating) {
            // restart sync time to default value
            mIsUpdating = true;

            // start network update
            AsyncTask<Void, Void, Void> updateTask = new UpdateTask();
            updateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
        } else if (!DefaultApplication.isConnected(this)) {
            Intent localIntent = new Intent(ACTION_BROADCAST);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
    }

    /**
     * Restarts the handler from the runnable based on the user-set
     * sync time.
     */
    private void startHandler() {
        // get the user set time
        int syncTime = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_INTEGER_SYNC,
                SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT);

        // if sync time is never, check back after certain
        // amount of time whether it has changed
        if (syncTime == SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER) {
            syncTime = SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT;
        }

        // remove the current runnable and add the new one
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, syncTime);
    }

    /**
     * Sets the repeatable runnable based on the current sync time.
     */
    private void setRunnable() {
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // get the user set time
                int syncTime = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_INTEGER_SYNC,
                        SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT);


                if (syncTime != SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER) {
                    update();
                } else {
                    // use default sync time if sync time is never
                    syncTime = SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT;
                }

                mHandler.postDelayed(this, syncTime);
            }
        };
    }

    /**
     * Checks whether the user has notifications enabled and, if so,
     * builds and displays a notification containing the current
     * unread count and a list of unread entries.
     */
    private void displayNotification() {
        // get current unread count and notification preferences
        boolean notify = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_NOTIFY,
                SharedPreferencesManager.VALUE_BOOLEAN_NOTIFY_DEFAULT);

        // only display if user wants notifications and there are new unread notifications
        if (notify) {
            // build the notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

            // set icon as vector or PNG depending on OS version
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setSmallIcon(R.drawable.ic_rss_feed_24dp);
            } else {
                builder.setSmallIcon(R.drawable.ic_rss);
            }

            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                    R.mipmap.ic_launcher));
            builder.setAutoCancel(true);
            builder.setOngoing(false);
            builder.setDefaults(Notification.DEFAULT_LIGHTS);

            // get the unread entries list
            List<Entry> entries = FeedDatabase.getUnreadEntries(null, 5);
            int size = FeedDatabase.getUnreadCount();
            String contentTitle = String.valueOf(size) + " ";
            StringBuilder contentText = new StringBuilder();

            // display singular or pluralized message depending on
            // the number of unread entries
            if (size == 1) {
                contentTitle += this.getString(R.string.popup_notification_unread_entry);
            } else {
                contentTitle += this.getString(R.string.popup_notification_unread_entries);
            }

            // get expandable inbox style
            builder.setContentTitle(contentTitle);
            NotificationCompat.InboxStyle style = new android.support.v4.app.NotificationCompat.InboxStyle();

            // set a public notification to display non-privacy-sensitive version, so
            // only display the unread count
            NotificationCompat.Builder publicBuilder = new NotificationCompat.Builder(this);
            publicBuilder.setSmallIcon(R.drawable.ic_rss_feed_24dp);
            publicBuilder.setContentTitle(contentTitle);
            builder.setPublicVersion(publicBuilder.build());

            // format the entry names and titles for display
            for (int i = 0; i < entries.size(); i++) {
                Entry entry = entries.get(i);
                String line = entry.getName();

                if (entry.getTitle() != null) {
                    line += " — " + DefaultApplication.fromHtml(entry.getTitle());
                }

                contentText.append(line);
                contentText.append("\n");
                style.addLine(line);
            }

            // set the inbox style
            builder.setContentText(contentText.toString());
            builder.setStyle(style);

            // determine whether to set a notification sound
            String notificationSound = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_STRING_NOTIFICATION_URI,
                    SharedPreferencesManager.VALUE_STRING_NOTIFICATION_URI_DEFAULT);
            if (notificationSound != null) {
                builder.setSound(Uri.parse(notificationSound));
            }

            // determine whether to vibrate
            boolean vibrate = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_BOOLEAN_VIBRATE,
                    SharedPreferencesManager.VALUE_BOOLEAN_VIBRATE_DEFAULT);
            if (vibrate) {
                builder.setVibrate(new long[] {500, 500, 500});
            }

            // set a result intent and add it to the stack
            Intent resultIntent = new Intent(this, FeedActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(FeedActivity.class);
            stackBuilder.addNextIntent(resultIntent);

            // set the pending intent
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            // display the notification
            NotificationManager notificationManager =
                    (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(1, builder.build());
            }
        }
    }

    /**
     * Returns the bitmap that is retrieved from the URL if one
     * exists; if not, returns null.
     *
     * @return the bitmap retrieved from the URL
     */
    private Bitmap getBitmapFromUrl(String address) {
        try {
            // get the domain of the url
            URL url = new URL(address);
            String domain = url.getHost();

            // get the image from the url
            address = "https://icons.duckduckgo.com/ip2/" + domain + ".ico";
            InputStream input = new URL(address).openStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // failed to get the image from url
        return null;
    }

    /**
     * Keeps the number of entries per subscription (not counting
     * starred entries) to no more than 100.
     */
    private void trimEntries() {
        List<String> subscriptionIds = new ArrayList<>(FeedDatabase.getAllSubscriptionIds());
        List<String> starredEntryIds = new ArrayList<>(FeedDatabase.getStarredEntryIds());

        // get all subscriptions
        for (int i = 0; i < subscriptionIds.size(); i++) {
            String subscriptionId = subscriptionIds.get(i);
            List<String> entryIds =
                    new ArrayList<>(FeedDatabase.getEntryIdsBySubscription(subscriptionId));

            // go through each entry of a subscription
            // that has more than 100 entries
            for (int v = 100; v < entryIds.size(); v++) {
                String entryId = entryIds.get(v);
                if (!starredEntryIds.contains(entryId)) {
                    FeedDatabase.deleteEntry(entryId);
                }
            }
        }
    }

    /**
     * Updates all of the subscriptions from the server.
     */
    private void updateSubscriptions() throws IOException, JSONException {
        JSONArray jsonSubscriptions = Feedbin.getSubscriptions();

        // do not process if array is null
        if (jsonSubscriptions == null) {
            return;
        }

        // loop through the subscriptions
        for (int i = 0; i < jsonSubscriptions.length(); i++) {
            JSONObject jsonSub = jsonSubscriptions.getJSONObject(i);

            // determine whether a subscription was added
            if (!FeedDatabase.containsSubscription(jsonSub.getString(Feedbin.JSON_FEED_ID))) {
                Subscription subscription = new Subscription();

                subscription.setId(jsonSub.getString(Feedbin.JSON_FEED_ID));
                subscription.setName(jsonSub.getString(Feedbin.JSON_TITLE));
                subscription.setLink(jsonSub.getString(Feedbin.JSON_FEED_URL));
                subscription.setIcon(getBitmapFromUrl(jsonSub.getString(Feedbin.JSON_SITE_URL)));
                FeedDatabase.createSub(subscription);
            } else {
                // determine whether a subscription was renamed
                Subscription subscription
                        = FeedDatabase.getSubscription(String.valueOf(jsonSub.getInt(Feedbin.JSON_FEED_ID)));
                if (!subscription.getName().equals(jsonSub.getString(Feedbin.JSON_TITLE))) {
                    subscription.setName(jsonSub.getString(Feedbin.JSON_TITLE));
                    FeedDatabase.updateSubscription(subscription);
                }
            }
        }

        // get current subscriptions from local database
        List<String> subscriptionIds = new ArrayList<>(FeedDatabase.getAllSubscriptionIds());
        boolean isDeleted = true;

        // determine whether a subscription was deleted
        for (int i = 0; i < subscriptionIds.size(); i++) {
            String subscriptionId = subscriptionIds.get(i);
            for (int v = 0; v < jsonSubscriptions.length(); v++) {
                JSONObject jsonSub = jsonSubscriptions.getJSONObject(v);
                if (subscriptionId.equals(jsonSub.getString(Feedbin.JSON_FEED_ID))) {
                    // the subscription was not deleted
                    isDeleted = false;
                    break;
                }
            }

            if (isDeleted) {
                // the subscription was deleted
                FeedDatabase.deleteSubscription(subscriptionId);
            }

            // reset boolean value
            isDeleted = true;
        }
    }

    /**
     * Updates all of the tags from the server.
     */
    private void updateTags() throws JSONException, IOException {
        // get the tags and subscriptions
        JSONArray jsonTags = Feedbin.getTags();
        JSONArray jsonSubs = Feedbin.getSubscriptions();

        // do not process if arrays are null
        if (jsonTags == null || jsonSubs == null) {
            return;
        }

        // loop through the tags
        for (int i = 0; i < jsonTags.length(); i++) {
            JSONObject jsonTag = jsonTags.getJSONObject(i);
            // determine whether a tag was added
            if (!FeedDatabase.containsTagByName(jsonTag.getString(Feedbin.JSON_NAME))) {
                String tagName = jsonTag.getString(Feedbin.JSON_NAME);
                List<Subscription> subscriptions = new ArrayList<>();
                Tag tag = new Tag(tagName, subscriptions);
                tag.setId(jsonTag.getString(Feedbin.JSON_ID));
                FeedDatabase.createTag(tag);
            }

            String tagId = FeedDatabase.getTagIdByName(jsonTag.getString(Feedbin.JSON_NAME));
            // determine whether a new relationship exists between
            // a subscription and a tag
            for (int v = 0; v < jsonSubs.length(); v++) {
                JSONObject jsonSub = jsonSubs.getJSONObject(v);
                String subId = jsonSub.getString(Feedbin.JSON_FEED_ID);
                if (jsonSub.getInt(Feedbin.JSON_FEED_ID) == jsonTag.getInt(Feedbin.JSON_FEED_ID)
                        && !FeedDatabase.containsCategorizedAs(tagId, subId)) {
                    FeedDatabase.createCategorizedAs(tagId, subId);
                    break;
                }
            }
        }

        // get current tags from local database
        List<String> tagNames = new ArrayList<>(FeedDatabase.getAllTagNames());
        boolean isTagDeleted = true;
        boolean isUnsubbed = true;

        // determine whether a tag was deleted
        for (int i = 0; i < tagNames.size(); i++) {
            String tagName = tagNames.get(i);
            for (int v = 0; v < jsonTags.length(); v++) {
                JSONObject jsonTag = jsonTags.getJSONObject(v);
                if (tagName.equals(jsonTag.getString(Feedbin.JSON_NAME))) {
                    // the tag was not deleted
                    isTagDeleted = false;
                }
            }

            if (isTagDeleted) {
                // the tag was deleted
                String tagId = FeedDatabase.getTagIdByName(tagName);
                FeedDatabase.deleteTag(tagId);
            } else {
                // determine whether a relationship between
                // a tag and a subscription was deleted
                String tagId = FeedDatabase.getTagIdByName(tagName);
                List<String> subscriptionIds =
                        new ArrayList<>(FeedDatabase.getSubscriptionIdsByTag(tagId));
                for (int v = 0; v < subscriptionIds.size(); v++) {
                    String subscriptionId = subscriptionIds.get(v);
                    for (int x = 0; x < jsonTags.length(); x++) {
                        JSONObject jsonTag = jsonTags.getJSONObject(x);
                        if (jsonTag.getString(Feedbin.JSON_FEED_ID).equals(subscriptionId)
                                && tagName.equals(jsonTag.getString(Feedbin.JSON_NAME))) {
                            // the relationship was not deleted
                            isUnsubbed = false;
                            break;
                        }
                    }

                    if (isUnsubbed) {
                        // the relationship was deleted
                        FeedDatabase.deleteCategorizedAs(tagId, subscriptionId);
                    }

                    // reset boolean values
                    isUnsubbed = true;
                }
            }

            // reset boolean value
            isTagDeleted = true;
        }
    }

    /**
     * Updates all of the entries from the server.
     */
    private void updateAllEntries() throws JSONException, IOException {
        // get the since parameter
        String since = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_SINCE,
                SharedPreferencesManager.VALUE_STRING_SINCE_DEFAULT);

        boolean firstLogin = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN,
                SharedPreferencesManager.VALUE_BOOLEAN_FIRST_LOGIN_DEFAULT);

        // get all entries since a certain date
        JSONArray jsonAllEntries = Feedbin.getAllEntries(since);

        // do not process if array is null
        if (jsonAllEntries == null) {
            return;
        }

        since = jsonAllEntries.getJSONObject(0).getString(Feedbin.JSON_CREATED_AT);
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SINCE, since);

        // add the new entries if not already added
        for (int i = 0; i < jsonAllEntries.length(); i++) {
            JSONObject jsonEntry = jsonAllEntries.getJSONObject(i);
            Entry entry = new Entry();

            entry.setId(jsonEntry.getString(Feedbin.JSON_ID));
            entry.setSubscriptionId(jsonEntry.getString(Feedbin.JSON_FEED_ID));
            entry.setDate(jsonEntry.getString(Feedbin.JSON_PUBLISHED));
            entry.setLink(jsonEntry.getString(Feedbin.JSON_URL));

            if (!jsonEntry.isNull(Feedbin.JSON_TITLE)) {
                entry.setTitle(jsonEntry.getString(Feedbin.JSON_TITLE));
            }

            if (!jsonEntry.isNull(Feedbin.JSON_CONTENT)) {
                entry.setContent(jsonEntry.getString(Feedbin.JSON_CONTENT));
            }

            if (!jsonEntry.isNull(Feedbin.JSON_AUTHOR)) {
                entry.setAuthor(jsonEntry.getString(Feedbin.JSON_AUTHOR));
            }

            // get the RSS enclosure if there is one
            if (jsonEntry.has(Feedbin.JSON_ENCLOSURE)) {
                JSONObject jsonEnclosure = jsonEntry.getJSONObject(Feedbin.JSON_ENCLOSURE);
                entry.setEnclosure(jsonEnclosure.getString(Feedbin.JSON_ENCLOSURE_URL));
            }

            if (!FeedDatabase.containsEntry(entry.getId())) {
                // add the new entry to the database
                FeedDatabase.createEntry(entry);
            } else {
                // update the already-present entry
                // and preserve read/starred values
                entry.setRead(FeedDatabase.entryIsRead(entry.getId()));
                entry.setStarred(FeedDatabase.entryIsStarred(entry.getId()));
                FeedDatabase.updateEntry(entry);
            }

            if (firstLogin && i > 1000) {
                break;
            }
        }

        // limit the number of entries per
        // subscription to 100
        trimEntries();
    }

    /**
     * Updates all of the starred entries from the server.
     */
    private void updateStarredEntries() throws JSONException, IOException {
        // get the starred entries
        JSONArray jsonStarredEntries = Feedbin.getStarredEntries();

        if (jsonStarredEntries == null) {
            return;
        }

        // check if there were starred entries added
        for (int i = 0; i < jsonStarredEntries.length(); i++) {
            String entryId = String.valueOf(jsonStarredEntries.get(i));
            if (FeedDatabase.containsEntry(entryId)) {
                // starred entry is already in database
                Entry entry = FeedDatabase.getEntry(entryId);
                if (!entry.isStarred()) {
                    entry.setStarred(true);
                    FeedDatabase.updateEntry(entry);
                }
            } else {
                // starred entry is not in database; fetch it
                JSONArray starredArray = Feedbin.getEntry(entryId);
                JSONObject starredObject = starredArray.getJSONObject(0);

                Entry entry = new Entry();

                entry.setId(starredObject.getString(Feedbin.JSON_ID));
                entry.setSubscriptionId(starredObject.getString(Feedbin.JSON_FEED_ID));
                entry.setDate(starredObject.getString(Feedbin.JSON_PUBLISHED));
                entry.setLink(starredObject.getString(Feedbin.JSON_URL));
                entry.setStarred(true);

                if (!starredObject.isNull(Feedbin.JSON_TITLE)) {
                    entry.setTitle(starredObject.getString(Feedbin.JSON_TITLE));
                }

                if (!starredObject.isNull(Feedbin.JSON_CONTENT)) {
                    entry.setContent(starredObject.getString(Feedbin.JSON_CONTENT));
                }

                if (!starredObject.isNull(Feedbin.JSON_AUTHOR)) {
                    entry.setAuthor(starredObject.getString(Feedbin.JSON_AUTHOR));
                }

                // get the RSS enclosure if there is one
                if (starredObject.has(Feedbin.JSON_ENCLOSURE)) {
                    JSONObject jsonEnclosure = starredObject.getJSONObject(Feedbin.JSON_ENCLOSURE);
                    entry.setEnclosure(jsonEnclosure.getString(Feedbin.JSON_ENCLOSURE_URL));
                }

                // get the name of the entry if it exists (starred
                // entries can have no corresponding subscription)
                if (FeedDatabase.containsSubscription(entry.getSubscriptionId())) {
                    Subscription subscription = FeedDatabase.getSubscription(entry.getSubscriptionId());
                    entry.setName(subscription.getName());

                    // add the new entry to the database if it
                    // has a corresponding subscription
                    FeedDatabase.createEntry(entry);
                }
            }
        }

        // get current starred entries from the local database
        List<String> starredEntryIds = new ArrayList<>(FeedDatabase.getStarredEntryIds());
        boolean isStarred = false;

        // check if there were starred entries removed
        for (int i = 0; i < starredEntryIds.size(); i++) {
            String starredEntryId = starredEntryIds.get(i);
            for (int v = 0; v < jsonStarredEntries.length(); v++) {
                String entryId = String.valueOf(jsonStarredEntries.get(v));
                if (starredEntryId.equals(entryId)) {
                    // the starred entry was not removed
                    isStarred = true;
                    break;
                }
            }

            if (!isStarred) {
                // the starred entry was removed
                Entry entry = FeedDatabase.getEntry(starredEntryId);
                entry.setStarred(false);
                FeedDatabase.updateEntry(entry);
            }

            // reset the boolean value
            isStarred = false;
        }
    }

    /**
     * Updates all of the unread entries from the server.
     */
    private void updateUnreadEntries() throws IOException, JSONException {
        // set the boolean and get the unread entries
        boolean newEntries = false;
        JSONArray jsonUnreadEntries = Feedbin.getUnreadEntries();

        if (jsonUnreadEntries == null) {
            return;
        }

        // check if unread entries have been added
        for (int i = 0; i < jsonUnreadEntries.length(); i++) {
            String entryId = String.valueOf(jsonUnreadEntries.get(i));
            if (FeedDatabase.containsEntry(entryId) && FeedDatabase.entryIsRead(entryId)) {
                // there are new unread entries
                FeedDatabase.markEntryAsUnread(entryId);
                newEntries = true;
            }
        }

        // get current unread entries from the local database
        List<String> unreadEntryIds = new ArrayList<>(FeedDatabase.getUnreadEntryIds());
        boolean isRead = true;

        // check to see if unread entries were removed
        for (int i = 0; i < unreadEntryIds.size(); i++) {
            String unreadEntryId = unreadEntryIds.get(i);
            for (int v = 0; v < jsonUnreadEntries.length(); v++) {
                String entryId = String.valueOf(jsonUnreadEntries.get(v));
                if (unreadEntryId.equals(entryId)) {
                    // the unread entry was not removed
                    isRead = false;
                    break;
                }
            }

            if (isRead) {
                // the unread entry was removed (marked as read)
                Entry entry = FeedDatabase.getEntry(unreadEntryId);
                entry.setRead(true);
                FeedDatabase.updateEntry(entry);
            }

            // reset the boolean value
            isRead = true;
        }

        if (newEntries) {
            displayNotification();
        }
    }

    /**
     * AsyncTask that updates the local database from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                updateSubscriptions();
                updateTags();
                updateAllEntries();
                updateStarredEntries();
                updateUnreadEntries();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mIsUpdating = false;
            Intent localIntent = new Intent(ACTION_BROADCAST);
            LocalBroadcastManager.getInstance(UpdateService.this).sendBroadcast(localIntent);
        }
    }
}
