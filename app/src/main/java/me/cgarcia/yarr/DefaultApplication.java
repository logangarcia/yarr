/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatDelegate;
import android.text.Html;
import android.text.format.DateUtils;
import android.webkit.WebView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import me.cgarcia.yarr.api.Feedbin;

/**
 * Extends the Application class by initializing common values
 * and managing shared methods between other classes.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class DefaultApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // instantiate the shared preferences and database
        SharedPreferencesManager.init(this);
        FeedDatabase.init(this);

        // get the first login value
        boolean firstLogin = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN,
                SharedPreferencesManager.VALUE_BOOLEAN_FIRST_LOGIN_DEFAULT);

        // start service if not the first time logging in and
        // the service is not already running
        if (!firstLogin && !isMyServiceRunning(UpdateService.class)) {
            Intent intent = new Intent(getApplicationContext(), UpdateService.class);
            this.startService(intent);
        }

        // get boolean value of whether night mode is active
        boolean isNight = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_NIGHT,
                SharedPreferencesManager.VALUE_BOOLEAN_NIGHT_DEFAULT);

        // set night mode accordingly
        if (isNight) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        // load dummy webview once to avoid day/night bug
        if (Build.VERSION.SDK_INT >= 24) {
            new WebView(this);
        }
    }

    /**
     * Provides different fromHtml methods depending on the
     * version of the operating system.
     *
     * @param data the data to convert to HTML
     * @return     the converted Html data
     */
    @SuppressWarnings("deprecation")
    public static String fromHtml(String data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(data).toString();
        }
    }

    /**
     * Provides different encode methods depending on the
     * version of the operating system.
     *
     * @param data the data to encode
     * @return     the encoded data
     */
    @SuppressWarnings("deprecation")
    public static String getEncodedUrl(String data) {
        try {
            // attempt to return modern URL encoder
            return URLEncoder.encode(
                    data, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // default to return legacy URL encoder
        return URLEncoder.encode(data).replaceAll("\\+", "%20");
    }

    /**
     * Returns the relative date from an absolute date.
     *
     * @param date the absolute date
     * @return     the relative date
     */
    @SuppressLint("SimpleDateFormat")
    public static String getRelativeTimeSpanString(String date) {
        // format date value
        Date dateTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(Feedbin.DATE_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            // parse the date based on the format
            dateTime = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // get the string value of the relative date
        return (String) DateUtils.getRelativeTimeSpanString(dateTime.getTime());
    }

    /**
     * Returns the new bitmap that is formatted from
     * the original one, if it exists.
     *
     * @return the formatted bitmap
     */
    public static Bitmap formatBitmap(Bitmap bitmap, Context context) {
        // get rss icon drawable
        Resources resources = context.getResources();
        Resources.Theme theme = context.getTheme();
        Drawable drawable = VectorDrawableCompat.create(resources, R.drawable.ic_rss_feed_24dp, theme);

        if (bitmap != null) {
            // create white background for bitmap
            Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                    bitmap.getConfig());
            Canvas canvas = new Canvas(newBitmap);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 0, null);

            // resize the bitmap for consistent dimensions
            return Bitmap.createScaledBitmap(newBitmap,
                    drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), false);
        }

        // no bitmap to parse; create new default bitmap
        Bitmap defaultBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(defaultBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        // create white background for bitmap
        Bitmap newBitmap = Bitmap.createBitmap(defaultBitmap.getWidth(), defaultBitmap.getHeight(),
                defaultBitmap.getConfig());
        canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(defaultBitmap, 0, 0, null);

        // return default bitmap
        return Bitmap.createScaledBitmap(newBitmap,
                drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), false);
    }

    /**
     * Returns whether there is a network connection based
     * on both the connection itself and user preferences.
     *
     * @param context the context of the activity
     * @return        the connection status
     */
    public static boolean isConnected(Context context) {
        // get the connectivity manager
        ConnectivityManager cm = (ConnectivityManager)
                context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        // get the status of the network connection
        NetworkInfo activeNetwork = null;

        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }

        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        // check user values only if there is a connection
        if (isConnected) {
            // connect only using wifi if enabled in preferences
            boolean wiFiOnly = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_BOOLEAN_WIFI,
                    SharedPreferencesManager.VALUE_BOOLEAN_WIFI_DEFAULT);

            if (wiFiOnly) {
                // set connection status based on wifi connection
                isConnected = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
            }
        }

        // connect only while charging if enabled in preferences
        boolean chargingOnly = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_CHARGING,
                SharedPreferencesManager.VALUE_BOOLEAN_CHARGING_DEFAULT);

        if (chargingOnly) {
            // get the intent of battery status
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, ifilter);

            // get the actual status
            if (batteryStatus != null) {
                int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;

                // connect only if still connected and device
                // is currently charging
                isConnected = isConnected && isCharging;
            }
        }

        return isConnected;
    }

    /**
     * Returns whether to grant storage permissions to an
     * invoked action.
     *
     * @param context the context of the activity
     * @return        the value of the storage permission
     */
    public static boolean isStoragePermissionGranted(Context context) {
        Activity activity = (Activity) context;
        if (Build.VERSION.SDK_INT >= 23) {
            if (context.getApplicationContext().checkSelfPermission(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                // user has enabled storage access
                return true;
            } else {
                // request storage access because user
                // has not enabled it
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            // permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    /**
     * Initializes the relevant API with its corresponding
     * credentials.
     */
    public static void setupApi() {
        String email = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_EMAIL,
                SharedPreferencesManager.VALUE_STRING_EMAIL_DEFAULT);
        String password = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_PASSWORD,
                SharedPreferencesManager.VALUE_STRING_PASSWORD_DEFAULT);

        // authenticate Feedbin with email and password
        Feedbin.init(email, password);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }

        return false;
    }
}
