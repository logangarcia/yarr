/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Holds information related to a tag.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Tag extends ExpandableGroup<Subscription> {

    private String id;
    private int count;

    /**
     * The default constructor.
     *
     * @param tagName  the name of the tag
     * @param tagSubs  the subscriptions of the tag
     */
    public Tag(String tagName, List<Subscription> tagSubs) {
        super(tagName, tagSubs);

        id = null;
        count = 0;
    }

    /**
     * Returns the ID of the tag.
     *
     * @return the ID of the tag
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the number of unread entries
     * of the tag.
     *
     * @return the number of unread entries
     *         of the tag
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the ID of the tag.
     *
     * @param id the ID of the tag
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Sets the number of unread entries
     * of the tag.
     *
     * @param count the number of unread entries
     *              of the tag
     */
    public void setCount(int count) {
        this.count = count;
    }
}
