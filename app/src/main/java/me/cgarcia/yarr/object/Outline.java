/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

/**
 * Holds information related to an outline element
 * of an OPML file.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Outline {
    private String title;
    private String link;

    /**
     * The default constructor.
     *
     */
    public Outline() {
        title = null;
        link = null;
    }

    /**
     * Returns the title of the outline.
     *
     * @return the title of the outline
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the atom link of the outline.
     *
     * @return the atom link of the outline
     */
    public String getLink() {
        return link;
    }

    /**
     * Sets the title of the outline.
     *
     * @param title the title of the outline
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets the atom link of the outline.
     *
     * @param link the atom link of the outline
     */
    public void setLink(String link) {
        this.link = link;
    }
}
