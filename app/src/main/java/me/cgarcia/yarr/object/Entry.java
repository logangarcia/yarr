/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

/**
 * Holds information related to an entry.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Entry {

    private String id;
    private String subscriptionId;
    private String name;
    private String date;
    private String title;
    private String content;
    private String author;
    private String link;
    private String enclosure;
    private boolean isRead;
    private boolean isStarred;

    /**
     * The default constructor.
     */
    public Entry() {
        id = null;
        subscriptionId = null;
        name = null;
        date = null;
        title = null;
        content = null;
        author = null;
        link = null;
        enclosure = null;
        isRead = true;
        isStarred = false;
    }

    /**
     * Returns the ID of the entry.
     *
     * @return the ID of the entry
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the ID of the subscription.
     *
     * @return the ID of the subscription
     */
    public String getSubscriptionId() {
        return subscriptionId;
    }

    /**
     * Returns the name of the entry.
     *
     * @return the name of the entry
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the date of the entry.
     *
     * @return the date of the entry
     */
    public String getDate() {
        return date;
    }

    /**
     * Returns the title of the entry.
     *
     * @return the title of the entry
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the content of the entry.
     *
     * @return the content of the entry
     */
    public String getContent() {
        return content;
    }

    /**
     * Returns the author of the entry.
     *
     * @return the author of the entry
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Returns the URL of the entry.
     *
     * @return the URL of the entry
     */
    public String getLink() {
        return link;
    }

    /**
     * Returns the enclosure of the entry.
     *
     * @return the enclosure of the entry
     */
    public String getEnclosure() {
        return enclosure;
    }

    /**
     * Returns the read status of the entry.
     *
     * @return the read status of the entry
     */
    public boolean isRead() { return isRead; }

    /**
     * Returns the starred status of the entry.
     *
     * @return the starred status of the entry
     */
    public boolean isStarred() {
        return isStarred;
    }

    /**
     * Sets the ID of the entry.
     *
     * @param id the ID of the entry
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Sets the ID of the subscription.
     *
     * @param subscriptionId the ID of the entry
     */
    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * Sets the name of the entry.
     *
     * @param name the name of the entry
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the date of the entry.
     *
     * @param date the date of the entry
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Sets the title of the entry.
     *
     * @param title the title of the entry
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets the content of the entry.
     *
     * @param content the content of the entry
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Sets the author of the entry.
     *
     * @param author the author of the entry
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Sets the URL of the entry.
     *
     * @param link the URL of the entry
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Sets the enclosure of the entry.
     *
     * @param enclosure the RSS enclosure of the entry
     */
    public void setEnclosure(String enclosure) {
        this.enclosure = enclosure;
    }

    /**
     * Sets the read status of the entry.
     *
     * @param isRead the read status of the entry
     */
    public void setRead(boolean isRead) { this.isRead = isRead; }

    /**
     * Sets the starred status of the entry.
     *
     * @param isStarred the starred status of the entry
     */
    public void setStarred(boolean isStarred) {
        this.isStarred = isStarred;
    }
}
