/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Manages the SQLite database that holds entries, subscriptions,
 * and tags.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class FeedDatabase extends SQLiteOpenHelper {
    private static FeedDatabase mInstance;

    // database version
    private static final int DATABASE_VERSION = 1;

    // database name
    private static final String DATABASE_NAME = "Feeds";

    // table names
    private static final String TABLE_ENTRY = "entries";
    private static final String TABLE_SUB = "subscriptions";
    private static final String TABLE_CATEGORIZED_AS = "categorized_as";
    private static final String TABLE_TAG = "tags";
    private static final String TABLE_SEARCH = "search";

    // trigger names
    private static final String TRIGGER_SEARCH_INSERT_AFTER = "search_insert_after";
    private static final String TRIGGER_SEARCH_DELETE_BEFORE = "search_delete_before";
    private static final String TRIGGER_SEARCH_UPDATE_BEFORE = "search_update_before";
    private static final String TRIGGER_SEARCH_UPDATE_AFTER = "search_update_after";

    // common column names
    private static final String KEY_SUB_ID = "sub_id";
    private static final String KEY_TAG_ID = "tag_id";

    // tags table - column names
    private static final String KEY_TAG_NAME = "tag_name";

    // subscriptions table - column names
    private static final String KEY_SUB_NAME = "sub_name";
    private static final String KEY_SUB_URL = "sub_url";
    private static final String KEY_SUB_ICON = "sub_icon";

    // entries table - column names
    private static final String KEY_ENTRY_ID = "entry_id";
    private static final String KEY_ENTRY_DATE = "entry_date";
    private static final String KEY_ENTRY_TITLE = "entry_title";
    private static final String KEY_ENTRY_CONTENT = "entry_content";
    private static final String KEY_ENTRY_AUTHOR = "entry_author";
    private static final String KEY_ENTRY_URL = "entry_url";
    private static final String KEY_ENTRY_ENCLOSURE = "entry_enclosure";
    private static final String KEY_ENTRY_READ = "entry_read";
    private static final String KEY_ENTRY_STAR = "entry_star";

    // tags table create statement
    private static final String CREATE_TABLE_TAG = "CREATE TABLE " + TABLE_TAG + "("
            + KEY_TAG_ID + " TEXT PRIMARY KEY, "
            + KEY_TAG_NAME + " TEXT NOT NULL COLLATE NOCASE)";

    // categorized as create statement
    private static final String CREATE_TABLE_CATEGORIZED_AS = "CREATE TABLE "
            + TABLE_CATEGORIZED_AS + "("
            + KEY_TAG_ID + " TEXT, "
            + KEY_SUB_ID + " TEXT, PRIMARY KEY ("
            + KEY_TAG_ID + ", "
            + KEY_SUB_ID + "), "
            + "FOREIGN KEY(" + KEY_SUB_ID + ") REFERENCES " + TABLE_SUB + "(" + KEY_SUB_ID + "), "
            + "FOREIGN KEY(" + KEY_TAG_ID + ") REFERENCES " + TABLE_TAG + "(" + KEY_TAG_ID + "))";

    // subscription table create statement
    private static final String CREATE_TABLE_SUB = "CREATE TABLE " + TABLE_SUB + "("
            + KEY_SUB_ID + " TEXT PRIMARY KEY, "
            + KEY_SUB_NAME + " TEXT NOT NULL COLLATE NOCASE, "
            + KEY_SUB_URL + " TEXT NOT NULL, "
            + KEY_SUB_ICON + " BLOB)";

    // entry table create statement
    private static final String CREATE_TABLE_ENTRY = "CREATE TABLE " + TABLE_ENTRY + "("
            + KEY_ENTRY_ID + " TEXT PRIMARY KEY, "
            + KEY_ENTRY_DATE + " TEXT NOT NULL, "
            + KEY_ENTRY_TITLE + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_CONTENT + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_AUTHOR + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_URL + " TEXT NOT NULL, "
            + KEY_ENTRY_ENCLOSURE + " TEXT, "
            + KEY_ENTRY_READ + " INTEGER, "
            + KEY_ENTRY_STAR + " INTEGER, "
            + KEY_SUB_ID + " TEXT, "
            + "FOREIGN KEY(" + KEY_SUB_ID + ") REFERENCES " + TABLE_SUB + "(" + KEY_SUB_ID + "))";

    // search table create statement
    private static final String CREATE_TABLE_SEARCH = "CREATE VIRTUAL TABLE " + TABLE_SEARCH
            + " USING FTS4(content=\""
            + TABLE_ENTRY + "\", "
            + KEY_ENTRY_TITLE + ", "
            + KEY_ENTRY_CONTENT + ", "
            + KEY_ENTRY_AUTHOR + ")";

    // trigger search insert statement
    private static final String CREATE_TRIGGER_INSERT_AFTER = "CREATE TRIGGER "
            + TRIGGER_SEARCH_INSERT_AFTER + " AFTER INSERT ON "
            + TABLE_ENTRY + " BEGIN INSERT INTO " + TABLE_SEARCH + "("
            + "docid, "
            + KEY_ENTRY_TITLE + ", "
            + KEY_ENTRY_CONTENT + ", "
            + KEY_ENTRY_AUTHOR + ") VALUES ("
            + "NEW.rowid, NEW."
            + KEY_ENTRY_TITLE + ", NEW."
            + KEY_ENTRY_CONTENT + ", NEW."
            + KEY_ENTRY_AUTHOR + "); END";

    private static final String CREATE_TRIGGER_DELETE_BEFORE = "CREATE TRIGGER "
            + TRIGGER_SEARCH_DELETE_BEFORE + " BEFORE DELETE ON "
            + TABLE_ENTRY + " BEGIN DELETE FROM "
            + TABLE_SEARCH + " WHERE docid=OLD.rowid; END";

    private static final String CREATE_TRIGGER_UPDATE_BEFORE = "CREATE TRIGGER "
            + TRIGGER_SEARCH_UPDATE_BEFORE + " BEFORE UPDATE ON "
            + TABLE_ENTRY + " BEGIN DELETE FROM "
            + TABLE_SEARCH + " WHERE docid=OLD.rowid; END";

    private static final String CREATE_TRIGGER_UPDATE_AFTER = "CREATE TRIGGER "
            + TRIGGER_SEARCH_UPDATE_AFTER + " AFTER UPDATE ON "
            + TABLE_ENTRY + " BEGIN INSERT INTO " + TABLE_SEARCH + "("
            + "docid, "
            + KEY_ENTRY_TITLE + ", "
            + KEY_ENTRY_CONTENT + ", "
            + KEY_ENTRY_AUTHOR + ") VALUES ("
            + "NEW.rowid, NEW."
            + KEY_ENTRY_TITLE + ", NEW."
            + KEY_ENTRY_CONTENT + ", NEW."
            + KEY_ENTRY_AUTHOR + "); END";

    /**
     * The default constructor.
     *
     * @param context the context of the activity
     */
    private FeedDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void init(Context context) {
        if (mInstance == null) {
            mInstance = new FeedDatabase(context.getApplicationContext());
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the tables and triggers
        db.execSQL(CREATE_TABLE_ENTRY);
        db.execSQL(CREATE_TABLE_SUB);
        db.execSQL(CREATE_TABLE_CATEGORIZED_AS);
        db.execSQL(CREATE_TABLE_TAG);
        db.execSQL(CREATE_TABLE_SEARCH);
        db.execSQL(CREATE_TRIGGER_INSERT_AFTER);
        db.execSQL(CREATE_TRIGGER_UPDATE_BEFORE);
        db.execSQL(CREATE_TRIGGER_DELETE_BEFORE);
        db.execSQL(CREATE_TRIGGER_UPDATE_AFTER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop the triggers and tables
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_INSERT_AFTER);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_UPDATE_BEFORE);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_DELETE_BEFORE);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_UPDATE_AFTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIZED_AS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);

        // create the tables again
        onCreate(db);
    }

    /**
     * Returns a list composed of all entries from
     * the database.
     *
     * @param date  the date at which to start the search
     * @param limit the number of entries to return
     * @return      the list of entries
     */
    public static List<Entry> getAllEntries(String date, int limit) {
        // declare a list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all unread entries
     * from the database.
     *
     * @param date  the date at which to start the search
     * @param limit the number of entries to return
     * @return      the list of unread entries
     */
    public static List<Entry> getUnreadEntries(String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_READ + " = " + 0
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_READ + " = " + 0
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all starred entries
     * from the database.
     *
     * @param date  the date at which to start the search
     * @param limit the number of entries to return
     * @return      the list of starred entries
     */
    public static List<Entry> getStarredEntries(String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * subscription from the database.
     *
     * @param subscriptionId the ID of the subscription
     * @param date           the date at which to start the search
     * @param limit          the number of entries to return
     * @return               the list of entries based on the subscription
     */
    public static List<Entry> getEntriesBySubscription(String subscriptionId, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId
                    + "' AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * tag from the database.
     *
     * @param date  the date at which to start the search
     * @param limit the number of entries to return
     * @param tagId the ID of the tag
     * @return      the list of entries based on the tag
     */
    public static List<Entry> getEntriesByTag(String tagId, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                    + KEY_SUB_ID + " IN (SELECT "
                    + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                    + KEY_TAG_ID + " = '" + tagId + "')"
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime("
                    + KEY_ENTRY_DATE + ") DESC LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                    + KEY_SUB_ID + " IN (SELECT "
                    + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                    + KEY_TAG_ID + " = '" + tagId + "')"
                    + " ORDER BY datetime("
                    + KEY_ENTRY_DATE + ") DESC LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries that match a given
     * query from the database.
     *
     * @param query the query to match against     *
     * @param date  the date at which to start the search
     * @param limit the number of entries to return
     * @return      the list of entries matching the query
     */
    public static List<Entry> getEntriesByQuery(String query, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        query = query.replace("'", "''");
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE rowid IN (SELECT docid FROM "
                    + TABLE_SEARCH + " WHERE " + TABLE_SEARCH + " MATCH '" + query + "')"
                    + " AND " + KEY_ENTRY_DATE + " < '" + date + "'"
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE rowid IN (SELECT docid FROM "
                    + TABLE_SEARCH + " WHERE " + TABLE_SEARCH + " MATCH '" + query + "')"
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();

                entry.setId(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID)));
                entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                entry.setName(getSubscriptionName(entry.getSubscriptionId()));
                entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
                entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
                entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
                entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
                entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
                entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
                entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
                entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all subscriptions from
     * the database.
     *
     * @return the list of subscriptions
     */
    public static List<Subscription> getAllSubscriptions() {
        // declare the list and create the query
        List<Subscription> subscriptions = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_SUB
                + " ORDER BY " + KEY_SUB_NAME + " ASC";

        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // loop through all rows and get the values
        if (cursor.moveToFirst()) {
            do {
                Subscription subscription = new Subscription();

                subscription.setId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                subscription.setName(cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME)));
                subscription.setLink(cursor.getString(cursor.getColumnIndex(KEY_SUB_URL)));
                subscription.setCount(getSubscriptionCount(subscription.getId()));
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));

                if (image.length > 0) {
                    subscription.setIcon(BitmapFactory.decodeByteArray(image, 0, image.length));
                }

                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of untagged subscriptions
     * from the database.
     *
     * @return the list of untagged subscriptions
     */
    public static List<Subscription> getUntaggedSubscriptions() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " NOT IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + ") ORDER BY "
                + KEY_SUB_NAME + " ASC";

        // get the database and cursor
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Subscription> subscriptions = new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Subscription subscription = new Subscription();

                subscription.setId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                subscription.setName(cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME)));
                subscription.setLink(cursor.getString(cursor.getColumnIndex(KEY_SUB_URL)));
                subscription.setCount(getSubscriptionCount(subscription.getId()));
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));

                if (image.length > 0) {
                    subscription.setIcon(BitmapFactory.decodeByteArray(image, 0, image.length));
                }

                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of subscriptions based on a
     * tag from the database.
     *
     * @param tagId the ID of the tag
     * @return      the list of subscriptions based on the tag
     */
    public static List<Subscription> getSubscriptionsByTag(String tagId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "') ORDER BY " + KEY_SUB_NAME + " ASC";

        // get the database and cursor
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Subscription> subscriptions = new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Subscription subscription = new Subscription();

                subscription.setId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
                subscription.setName(cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME)));
                subscription.setLink(cursor.getString(cursor.getColumnIndex(KEY_SUB_URL)));
                subscription.setCount(getSubscriptionCount(subscription.getId()));
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));

                if (image.length > 0) {
                    subscription.setIcon(BitmapFactory.decodeByteArray(image, 0, image.length));
                }

                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of all tags from
     * the database.
     *
     * @return the list of tags
     */
    public static List<Tag> getAllTags() {
        // declare the list and create the query
        List<Tag> tags = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TAG
                + " ORDER BY " + KEY_TAG_NAME + " ASC";

        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // loop through and get the values
        if (cursor.moveToFirst()) {
            do {
                String tagId = cursor.getString(cursor.getColumnIndex(KEY_TAG_ID));
                String tagName = cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME));
                List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));

                Tag tag = new Tag(tagName, subscriptions);
                tag.setId(tagId);
                tag.setCount(getTagCount(tag.getId()));
                tags.add(tag);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return tags;
    }

    /**
     * Returns a list of all entry IDs from
     * the database.
     *
     * @return the list of entry IDs
     */
    public static List<String> getAllEntryIds() {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_ENTRY_ID + " FROM " + TABLE_ENTRY
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of unread entry IDs from
     * the database.
     *
     * @return the list of entry IDs
     */
    public static List<String> getUnreadEntryIds() {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_ENTRY_ID + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_READ + " = " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of starred entry IDs from
     * the database.
     *
     * @return the list of entry IDs
     */
    public static List<String> getStarredEntryIds() {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_ENTRY_ID + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of all entry IDs related
     * to a subscription from the database.
     *
     * @param subscriptionId the ID of the subscription
     * @return               the list of entry IDs
     */
    public static List<String> getEntryIdsBySubscription(String subscriptionId) {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_ENTRY_ID + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId
                + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of all entry IDs related
     * to a tag from the database.
     *
     * @param tagId the ID of the tag
     * @return      the list of entry IDs
     */
    public static List<String> getEntryIdsByTag(String tagId) {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_ENTRY_ID + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "') ORDER BY datetime("
                + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of all entry IDs related
     * to a query from the database.
     *
     * @param query the user query
     * @return      the list of entry IDs
     */
    public static List<String> getEntryIdsByQuery(String query) {
        // declare a list and create the query
        List<String> entryIds = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE rowid IN (SELECT docid FROM "
                + TABLE_SEARCH + " WHERE " + TABLE_SEARCH + " MATCH '" + query + "')"
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                entryIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entryIds;
    }

    /**
     * Returns a list of all subscription
     * IDs from the database
     *
     * @return the list of subscription IDs
     */
    public static List<String> getAllSubscriptionIds() {
        // declare a list and create the query
        List<String> subscriptionIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_SUB_ID + " FROM " + TABLE_SUB
                + " ORDER BY " + KEY_SUB_NAME + " ASC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                subscriptionIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptionIds;
    }

    /**
     * Returns a list of subscription
     * IDs related by tag from the database
     *
     * @param tagId the ID of the tag
     * @return      the list of subscription IDs
     */
    public static List<String> getSubscriptionIdsByTag(String tagId) {
        // declare a list and create the query
        List<String> subscriptionIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_SUB_ID + " FROM "
                + TABLE_SUB + " WHERE " + KEY_SUB_ID + " IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "') ORDER BY " + KEY_SUB_NAME + " ASC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                subscriptionIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptionIds;
    }

    /**
     * Returns a list of all tag names from the database
     *
     * @return the list of tag names
     */
    public static List<String> getAllTagNames() {
        // declare a list and create the query
        List<String> tagIds = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_TAG_NAME + " FROM " + TABLE_TAG
                + " ORDER BY " + KEY_TAG_NAME + " ASC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                tagIds.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return tagIds;
    }

    /**
     * Returns a list of tag names related to
     * a subscription from the database
     *
     * @param subscriptionId the ID of the subscription
     * @return               the list of tag names
     */
    public static List<String> getTagNamesBySubscription(String subscriptionId) {
        // declare a list and create the query
        List<String> tagNames = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_TAG_NAME + " FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_ID + " IN (SELECT "
                + KEY_TAG_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_SUB_ID + " = '" + subscriptionId + "') ORDER BY " + KEY_TAG_NAME + " ASC";

        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                tagNames.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return tagNames;
    }

    /**
     * Returns an entry from the database.
     *
     * @param entryId the ID of the entry to retrieve
     * @return        the entry object
     */
    public static Entry getEntry(String entryId) {
        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_ENTRY_ID + " = '" + entryId + "'";

        // set the cursor based on the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        Entry entry = new Entry();

        if (cursor.moveToFirst()) {
            // retrieve elements from table
            entry.setId(entryId);
            entry.setSubscriptionId(cursor.getString(cursor.getColumnIndex(KEY_SUB_ID)));
            entry.setName(getSubscriptionName(entry.getSubscriptionId()));
            entry.setDate(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE)));
            entry.setTitle(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE)));
            entry.setContent(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT)));
            entry.setAuthor(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR)));
            entry.setLink(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL)));
            entry.setEnclosure(cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE)));
            entry.setRead(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0);
            entry.setStarred(cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0);
        }

        // close the cursor and return the object
        cursor.close();
        return entry;
    }

    /**
     * Returns a subscription from the database.
     *
     * @param subscriptionId the ID of the subscription to retrieve
     * @return               the subscription object
     */
    public static Subscription getSubscription(String subscriptionId) {
        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " = '" + subscriptionId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        Subscription subscription = new Subscription();

        if (cursor.moveToFirst()) {
            // get the elements
            subscription.setId(subscriptionId);
            subscription.setName(cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME)));
            subscription.setLink(cursor.getString(cursor.getColumnIndex(KEY_SUB_URL)));
            subscription.setCount(getSubscriptionCount(subscriptionId));
            byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));

            if (image.length > 0) {
                subscription.setIcon(BitmapFactory.decodeByteArray(image, 0, image.length));
            }
        }

        // close the cursor and return the object
        cursor.close();
        return subscription;
    }

    /**
     * Returns a tag from the database.
     *
     * @param tagId the ID of the tag to retrieve
     * @return      the tag object
     */
    public static Tag getTag(String tagId) {
        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectTagQuery = "SELECT * FROM " + TABLE_TAG + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectTagQuery, null);
        Tag tag = null;

        if (cursor.moveToFirst()) {
            // get the values from the table
            String tagName = cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME));
            List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
            tag = new Tag(tagName, subscriptions);
            tag.setId(tagId);
            tag.setCount(getTagCount(tag.getId()));
        }

        // close the cursor and return the object
        cursor.close();
        return tag;
    }

    /**
     * Returns a tag from the database.
     *
     * @param tagName the name of the tag to retrieve
     * @return        the tag object
     */
    public static Tag getTagByName(String tagName) {
        // get the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectTagQuery = "SELECT * FROM " + TABLE_TAG + " WHERE "
                + KEY_TAG_NAME + " = '" + tagName + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectTagQuery, null);
        Tag tag = null;

        if (cursor.moveToFirst()) {
            // get the values from the table
            String tagId = cursor.getString(cursor.getColumnIndex(KEY_TAG_ID));
            List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
            tag = new Tag(tagName, subscriptions);
            tag.setId(tagId);
            tag.setCount(getTagCount(tag.getId()));
        }

        // close the cursor and return the object
        cursor.close();
        return tag;
    }

    /**
     * Returns the name of the subscription.
     *
     * @param  subscriptionId the ID of the subscription
     * @return                the name of the subscription
     */
    public static String getSubscriptionName(String subscriptionId) {
        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT " + KEY_SUB_NAME + " FROM " + TABLE_SUB
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        String subName = null;

        if (cursor.moveToFirst()) {
            // get the elements
            subName = cursor.getString(0);
        }

        // close the cursor and return the object
        cursor.close();
        return subName;
    }

    /**
     * Returns the ID of the tag.
     *
     * @param  tagName the name of the tag
     * @return         the ID of the tag
     */
    public static String getTagIdByName(String tagName) {
        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT " + KEY_TAG_ID + " FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_NAME + " = '" + tagName + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        String tagId = null;

        if (cursor.moveToFirst()) {
            // get the elements
            tagId = cursor.getString(0);
        }

        // close the cursor and return the object
        cursor.close();
        return tagId;
    }

    /**
     * Returns the name of the tag.
     *
     * @param  tagId the ID of the tag
     * @return       the name of the tag
     */
    public static String getTagName(String tagId) {
        // set the database
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT " + KEY_TAG_NAME + " FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        String tagName = null;

        if (cursor.moveToFirst()) {
            // get the elements
            tagName = cursor.getString(0);
        }

        // close the cursor and return the object
        cursor.close();
        return tagName;
    }

    /**
     * Creates an entry and stores it in the database.
     *
     * @param entry the entry object to store
     * @return      the status of the insertion
     */
    public static long createEntry(Entry entry) {
        // set the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues entryValues = new ContentValues();

        // put the values into the table
        entryValues.put(KEY_ENTRY_ID, entry.getId());
        entryValues.put(KEY_ENTRY_DATE, entry.getDate());
        entryValues.put(KEY_ENTRY_TITLE, entry.getTitle());
        entryValues.put(KEY_ENTRY_CONTENT, entry.getContent());
        entryValues.put(KEY_ENTRY_AUTHOR, entry.getAuthor());
        entryValues.put(KEY_ENTRY_URL, entry.getLink());
        entryValues.put(KEY_ENTRY_ENCLOSURE, entry.getEnclosure());
        entryValues.put(KEY_ENTRY_READ, entry.isRead());
        entryValues.put(KEY_ENTRY_STAR, entry.isStarred());
        entryValues.put(KEY_SUB_ID, entry.getSubscriptionId());

        // insert row
        return db.insert(TABLE_ENTRY, null, entryValues);
    }

    /**
     * Creates a subscription and stores it in the database.
     *
     * @param subscription the subscription object to store
     * @return             the status of the insertion
     */
    public static long createSub(Subscription subscription) {
        // set the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_SUB_ID, subscription.getId());
        values.put(KEY_SUB_NAME, subscription.getName());
        values.put(KEY_SUB_URL, subscription.getLink());
        Bitmap bitmap = subscription.getIcon();

        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            values.put(KEY_SUB_ICON, stream.toByteArray());
        } else {
            values.put(KEY_SUB_ICON, new byte[0]);
        }

        // insert row
        return db.insert(TABLE_SUB, null, values);
    }

    /**
     * Creates a relationship between a tag and a
     * subscription and stores it in the database.
     *
     * @param tagId          the ID of the tag
     * @param subscriptionId the ID of the subscription
     * @return               the status of the insertion
     */
    public static long createCategorizedAs(String tagId, String subscriptionId) {
        // set the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_TAG_ID, tagId);
        values.put(KEY_SUB_ID, subscriptionId);

        // insert row
        return db.insert(TABLE_CATEGORIZED_AS, null, values);
    }

    /**
     * Creates a tag and stores it in the database.
     *
     * @param tag the tag object to store
     * @return    the status of the insertion
     */
    public static long createTag(Tag tag) {
        // set the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_TAG_ID, tag.getId());
        values.put(KEY_TAG_NAME, tag.getTitle());

        // insert row
        return db.insert(TABLE_TAG, null, values);
    }

    /**
     * Returns the number of unread entries within the
     * database.
     *
     * @return the number of unread entries
     */
    public static int getUnreadCount() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_READ + " = " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database and cursor
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of starred entries within the
     * database.
     *
     * @return the number of starred entries
     */
    public static int getStarredCount() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database and cursor
        SQLiteDatabase db = mInstance.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of unread entries belonging to
     * a subscription within the database.
     *
     * @param subscriptionId the ID of the subscription
     * @return               the number of unread entries
     */
    public static int getSubscriptionCount(String subscriptionId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId + "' AND "
                + KEY_ENTRY_READ + " = " + 0;

        // get the database and cursor
        SQLiteDatabase db = mInstance.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of unread entries belonging to
     * a tag within the database.
     *
     * @param tagId the ID of the tag
     * @return      the number of unread entries
     */
    public static int getTagCount(String tagId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_SUB_ID + " IN (SELECT " + KEY_SUB_ID + " FROM "
                + TABLE_CATEGORIZED_AS + " WHERE " + KEY_TAG_ID + " = '" + tagId + "') AND "
                + KEY_ENTRY_READ + " = " + 0;

        // get the database and cursor
        SQLiteDatabase db = mInstance.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Updates a preexisting entry within the database.
     *
     * @param entry the updated entry
     * @return      the status of the update
     */
    public static int updateEntry(Entry entry) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues entryValues = new ContentValues();

        // set the values in the table
        entryValues.put(KEY_ENTRY_ID, entry.getId());
        entryValues.put(KEY_ENTRY_DATE, entry.getDate());
        entryValues.put(KEY_ENTRY_TITLE, entry.getTitle());
        entryValues.put(KEY_ENTRY_CONTENT, entry.getContent());
        entryValues.put(KEY_ENTRY_AUTHOR, entry.getAuthor());
        entryValues.put(KEY_ENTRY_URL, entry.getLink());
        entryValues.put(KEY_ENTRY_ENCLOSURE, entry.getEnclosure());
        entryValues.put(KEY_ENTRY_READ, entry.isRead());
        entryValues.put(KEY_ENTRY_STAR, entry.isStarred());
        entryValues.put(KEY_SUB_ID, entry.getSubscriptionId());

        // update the row
        return db.update(TABLE_ENTRY, entryValues, KEY_ENTRY_ID + " = ?",
                new String[] { entry.getId() });
    }

    /**
     * Updates a preexisting subscription within the database.
     *
     * @param subscription the updated subscription
     * @return             the status of the update
     */
    public static int updateSubscription(Subscription subscription) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        ContentValues values = new ContentValues();

        // set the values in the table
        values.put(KEY_SUB_ID, subscription.getId());
        values.put(KEY_SUB_NAME, subscription.getName());
        values.put(KEY_SUB_URL, subscription.getLink());

        // update the row
        return db.update(TABLE_SUB, values, KEY_SUB_ID + " = ?",
                new String[] { subscription.getId() });
    }

    /**
     * Deletes an entry from the database.
     *
     * @param entryId the ID of the entry to delete
     * @return        the status of the deletion
     */
    public static int deleteEntry(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // delete the row
        return db.delete(TABLE_ENTRY, KEY_ENTRY_ID + " = ?",
                new String[] { String.valueOf(entryId) });
    }

    /**
     * Deletes a subscription from the database.
     *
     * @param subscriptionId the ID of the subscription to delete
     * @return               the status of the deletion
     */
    public static int deleteSubscription(String subscriptionId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();
        deleteEntriesBySubscription(subscriptionId);

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId + "'";

        // delete relationship between subscription and tag
        db.execSQL(query);

        // delete the row
        return db.delete(TABLE_SUB, KEY_SUB_ID + " = ?",
                new String[] { String.valueOf(subscriptionId) });
    }

    /**
     * Deletes a tag from the database.
     *
     * @param tagId the ID of the tag to delete
     * @return      the status of the deletion
     */
    public static int deleteTag(String tagId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";

        // delete the relationship between the tag and subscriptions
        db.execSQL(query);

        // delete the row
        return db.delete(TABLE_TAG, KEY_TAG_ID + " = ?",
                new String[] { String.valueOf(tagId) });
    }

    /**
     * Determines whether an entry exists in the database.
     *
     * @param entryId the ID of the entry
     * @return        the boolean existence of the entry
     */
    public static boolean containsEntry(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a subscription exists
     * in the database.
     *
     * @param subscriptionId the ID of the subscription
     * @return               the boolean existence of the subscription
     */
    public static boolean containsSubscription(String subscriptionId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_SUB
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a relationship between a
     * subscription and a tag exists in the database.
     *
     * @param tagId          the ID of the tag
     * @param subscriptionId the ID of the subscription
     * @return               the boolean existence of the relationship
     */
    public static boolean containsCategorizedAs(String tagId, String subscriptionId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "' AND "
                + KEY_SUB_ID + " = '" + subscriptionId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether an tag exists in the database.
     *
     * @param tagId the ID of the tag
     * @return      the boolean existence of the tag
     */
    public static boolean containsTag(String tagId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a tag exists in the database.
     *
     * @param tagName the name of the tag
     * @return        the boolean existence of the tag
     */
    public static boolean containsTagByName(String tagName) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_NAME + " = '" + tagName + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    public static boolean entryIsRead(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT " + KEY_ENTRY_READ + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean isRead = true;

        if (cursor.moveToFirst()) {
            // get the elements
            isRead = cursor.getInt(0) != 0;
        }

        cursor.close();
        return isRead;
    }

    public static boolean entryIsStarred(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "SELECT " + KEY_ENTRY_STAR + " FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean isStarred = false;

        if (cursor.moveToFirst()) {
            // get the elements
            isStarred = cursor.getInt(0) != 0;
        }

        cursor.close();
        return isStarred;
    }

    /**
     * Deletes a relationship between a tag and a
     * subscription from the database.
     *
     * @param tagId          the ID of the tag
     * @param subscriptionId the ID of the subscription
     */
    public static void deleteCategorizedAs(String tagId, String subscriptionId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "' AND "
                + KEY_SUB_ID + " = '" + subscriptionId + "'";

        // delete the relationships
        db.execSQL(query);
    }

    /**
     * Deletes entries of a subscription from the database.
     *
     * @param subscriptionId the ID of the subscription
     */
    public static void deleteEntriesBySubscription(String subscriptionId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subscriptionId + "'";

        // delete the rows
        db.execSQL(query);
    }

    public static void markEntryAsRead(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "UPDATE " + TABLE_ENTRY + " SET " + KEY_ENTRY_READ + " = " + 1
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";

        // update the row
        db.execSQL(query);
    }

    public static void markEntryAsUnread(String entryId) {
        // get the database
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // create the query
        String query = "UPDATE " + TABLE_ENTRY + " SET " + KEY_ENTRY_READ + " = " + 0
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";

        // update the row
        db.execSQL(query);
    }

    /**
     * Closes the database.
     */
    public static void closeDB() {
        SQLiteDatabase db = mInstance.getReadableDatabase();

        // check if database can be closed
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    /**
     * Deletes the database.
     */
    public static void deleteDB() {
        SQLiteDatabase db = mInstance.getWritableDatabase();

        // drop the triggers and tables
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_INSERT_AFTER);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_UPDATE_BEFORE);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_DELETE_BEFORE);
        db.execSQL("DROP TRIGGER IF EXISTS " + TRIGGER_SEARCH_UPDATE_AFTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIZED_AS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);

        // create the tables and triggers again
        db.execSQL(CREATE_TABLE_ENTRY);
        db.execSQL(CREATE_TABLE_SUB);
        db.execSQL(CREATE_TABLE_CATEGORIZED_AS);
        db.execSQL(CREATE_TABLE_TAG);
        db.execSQL(CREATE_TABLE_SEARCH);
        db.execSQL(CREATE_TRIGGER_INSERT_AFTER);
        db.execSQL(CREATE_TRIGGER_UPDATE_BEFORE);
        db.execSQL(CREATE_TRIGGER_DELETE_BEFORE);
        db.execSQL(CREATE_TRIGGER_UPDATE_AFTER);
    }
}
