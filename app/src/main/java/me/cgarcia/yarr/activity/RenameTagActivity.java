/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.fragment.RenameTagFragment;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity which manages the renaming of a tag.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class RenameTagActivity extends AppCompatActivity implements RenameTagFragment.TaskCallbacks {
    // string to identify the fragment
    private static final String KEY_STRING_FRAGMENT = "rename_tag_fragment";
    private static final String KEY_STRING_NAME = "name";
    private static final String KEY_STRING_TAG_ID = "tag_id";
    private static final String KEY_BOOLEAN_PROGRESS = "progress";

    // declare the fragment
    private RenameTagFragment mRenameTagFragment;

    // declare the database and progress
    private boolean mProgress;

    // store values from user in variables
    private String mOldName;
    private String mTagId;

    // declare layout fields
    private EditText mNameView;
    private View mProgressView;
    private View mRenameTagView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout and API
        setContentView(R.layout.activity_rename_tag);
        DefaultApplication.setupApi();

        // find the fragment
        FragmentManager fm = getFragmentManager();
        mRenameTagFragment = (RenameTagFragment) fm.findFragmentByTag(KEY_STRING_FRAGMENT);

        // create the fragment the first time
        if (mRenameTagFragment == null) {
            // add the fragment
            mRenameTagFragment = new RenameTagFragment();
            fm.beginTransaction().add(mRenameTagFragment, KEY_STRING_FRAGMENT).commit();
        }

        //set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_rename_tag);

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the layout fields
        mNameView = (EditText) findViewById(R.id.activity_rename_tag_field);
        mProgressView = findViewById(R.id.activity_rename_tag_progress);
        mRenameTagView = findViewById(R.id.activity_rename_tag_form);

        // get the current tag name
        mTagId = SharedPreferencesManager.getValue(SharedPreferencesManager.KEY_STRING_TAG_ID,
                SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
        Tag tag = FeedDatabase.getTag(mTagId);
        mOldName = tag.getTitle();

        // set the current tag name in the text field on startup
        if (savedInstanceState == null) {
            mNameView.setText(mOldName);
            mNameView.setSelection(mNameView.getText().length());
            mProgress = false;
        } else {
            mOldName = savedInstanceState.getString(KEY_STRING_NAME);
            mTagId = savedInstanceState.getString(KEY_STRING_TAG_ID);
            showProgress(savedInstanceState.getBoolean(KEY_BOOLEAN_PROGRESS));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save the current state
        outState.putString(KEY_STRING_NAME, mOldName);
        outState.putString(KEY_STRING_TAG_ID, mTagId);
        outState.putBoolean(KEY_BOOLEAN_PROGRESS, mProgress);
    }

    @Override
    public void onBackPressed() {
        // close activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rename_tag, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                break;
            // rename tag if not currently renaming
            case R.id.menu_rename_tag_done:
                if (!mProgress) {
                    attemptRenameTag();
                }
                break;
        }
        return true;
    }

    @Override
    public void onPostRenameTag() {
        // finish the activity
        setResult(AddSubscriptionActivity.RESULT_OK);
        finish();
    }

    /**
     * Renames a tag if the correct conditions are achieved.
     */
    private void attemptRenameTag() {
        // reset errors and get user input
        mNameView.setError(null);
        String newName = mNameView.getText().toString();

        // reset focus and cancel values
        boolean cancel = false;
        View focusView = null;

        // make sure required name is not empty
        if (TextUtils.isEmpty(newName)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else if (newName.equals(mOldName)) {
            // nothing was modified
            finish();
        } else if (!DefaultApplication.isConnected(this)) {
            // display error if no connection
            Snackbar.make(mRenameTagView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else {
            // show progress spinner
            showProgress(true);

            // delete old tag and create new tag with same subscriptions
            Tag oldTag = FeedDatabase.getTag(mTagId);
            List<String> subscriptionIds = new ArrayList<>(FeedDatabase.getSubscriptionIdsByTag(mTagId));
            Tag newTag = new Tag(newName, oldTag.getItems());
            newTag.setId(mTagId);
            FeedDatabase.deleteTag(mTagId);
            FeedDatabase.createTag(newTag);

            // add previous subscriptions to new tag
            for (int i = 0; i < subscriptionIds.size(); i++) {
                FeedDatabase.createCategorizedAs(mTagId, subscriptionIds.get(i));
            }

            // change the tag name on the network
            mRenameTagFragment.renameTag(mTagId, newName);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view layout if true
        mRenameTagView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRenameTagView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRenameTagView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
