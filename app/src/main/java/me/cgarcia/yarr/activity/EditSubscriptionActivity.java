/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.fragment.EditSubscriptionFragment;
import me.cgarcia.yarr.object.Subscription;

/**
 * Activity that manages the editing of current subscriptions.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class EditSubscriptionActivity extends AppCompatActivity implements
        EditSubscriptionFragment.TaskCallbacks {
    // string to identify the fragment
    private static final String KEY_STRING_FRAGMENT = "edit_subscription_fragment";
    private static final String KEY_BOOLEAN_PROGRESS = "progress";

    // declare the fragment
    private EditSubscriptionFragment mEditSubscriptionFragment;

    // declare subscription and tags
    //private Subscription mSubscription;
    private String mSubscriptionId;
    private String mNewTags;

    // determine whether there is loading
    private boolean mProgress;

    // the view fields of the layout
    private EditText mNameView;
    private MultiAutoCompleteTextView mTagsView;
    private TextView mTagsHelpView;
    private View mProgressView;
    private View mEditSubscriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout
        setContentView(R.layout.activity_edit_subscription);
        DefaultApplication.setupApi();

        // find the fragment
        FragmentManager fm = getFragmentManager();
        mEditSubscriptionFragment = (EditSubscriptionFragment) fm.findFragmentByTag(KEY_STRING_FRAGMENT);

        // create the fragment the first time
        if (mEditSubscriptionFragment == null) {
            // add the fragment
            mEditSubscriptionFragment = new EditSubscriptionFragment();
            fm.beginTransaction().add(mEditSubscriptionFragment, KEY_STRING_FRAGMENT).commit();
        }

        //set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_edit_subscription);

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the view fields
        mNameView = (EditText) findViewById(R.id.activity_edit_subscription_name);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_edit_subscription_tags);
        mProgressView = findViewById(R.id.activity_edit_subscription_progress);
        mEditSubscriptionView = findViewById(R.id.activity_edit_subscription);
        mTagsHelpView = (TextView) findViewById(R.id.activity_edit_subscription_help);

        // get the subscriptions and tags from the subscription ID
        mSubscriptionId = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_SUB_ID,
                SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
        //mSubscription = FeedDatabase.getSubscription(mSubscriptionId);
        List<String> subTags = FeedDatabase.getTagNamesBySubscription(mSubscriptionId);

        // set the tag names for auto-completion
        List<String> tagNames = new ArrayList<>(FeedDatabase.getAllTagNames());

        // set the tag field as comma-separated list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, tagNames);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_edit_subscription_tags);
        mTagsView.setAdapter(adapter);
        mTagsView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mTagsView.setOnFocusChangeListener(setupHelpView());

        // set the field values for the first time
        if (savedInstanceState == null) {
            //mNameView.setText(mSubscription.getName());
            mNameView.setText(FeedDatabase.getSubscriptionName(mSubscriptionId));
            mNameView.setSelection(mNameView.getText().length());
            StringBuilder subTagNames = new StringBuilder();

            // loop through the tag names and set them in the field
            for (int i = 0; i < subTags.size() - 1; i++) {
                subTagNames.append(subTags.get(i)).append(", ");
            }

            // last tag name does not have a comma at the end
            if (subTags.size() > 0) {
                subTagNames.append(subTags.get(subTags.size() - 1));
            }

            mTagsView.setText(subTagNames.toString());
            mTagsView.setSelection(mTagsView.getText().length());

            mProgress = false;
        } else {
            showProgress(savedInstanceState.getBoolean(KEY_BOOLEAN_PROGRESS));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save the current progress state
        outState.putBoolean(KEY_BOOLEAN_PROGRESS, mProgress);
    }

    @Override
    public void onBackPressed() {
        // finish the activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_subscription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                return true;
            // on completion button press
            case R.id.menu_edit_subscription_done:
                // do not call method if already called
                if (!mProgress) {
                    attemptEditSubscription();
                }
                break;
        }

        return true;
    }

    @Override
    public void onPostEditSubscriptionName() {
        // edit the tags
        mEditSubscriptionFragment.editSubscriptionTags(mSubscriptionId, mNewTags);
    }

    @Override
    public void onPostEditSubscriptionTags() {
        // finish the activity
        setResult(AddSubscriptionActivity.RESULT_OK);
        finish();
    }

    /**
     * Returns the focus change listener that shows/hides
     * the tag hint below the tag field.
     *
     * @return the on focus change listener
     */
    private View.OnFocusChangeListener setupHelpView() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // the tag view has focus
                if (hasFocus) {
                    mTagsHelpView.setVisibility(View.VISIBLE);
                } else {
                    mTagsHelpView.setVisibility(View.INVISIBLE);
                }
            }
        };
    }

    /**
     * Edits a subscription if the correct conditions are achieved.
     */
    private void attemptEditSubscription() {
        // reset the error fields
        mNameView.setError(null);
        mTagsView.setError(null);

        // get the user-provided values from the text fields
        String newName = mNameView.getText().toString();
        mNewTags = mTagsView.getText().toString();

        // reset cancel and focus
        boolean cancel = false;
        View focusView = null;

        // check if required name exists
        if (TextUtils.isEmpty(newName)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else if (!DefaultApplication.isConnected(this)) {
            // there is no network connection
            Snackbar.make(mEditSubscriptionView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else if (!FeedDatabase.getSubscriptionName(mSubscriptionId).equals(newName)) {
            // the new subscription name does not equal the old one, so
            // update the name
            showProgress(true);
            Subscription subscription = FeedDatabase.getSubscription(mSubscriptionId);
            subscription.setName(newName);
            FeedDatabase.updateSubscription(subscription);
            mEditSubscriptionFragment.editSubscriptionName(mSubscriptionId, newName);
        } else {
            // update the tags only
            showProgress(true);
            mEditSubscriptionFragment.editSubscriptionTags(mSubscriptionId, mNewTags);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view if true
        mEditSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
        mEditSubscriptionView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mEditSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
