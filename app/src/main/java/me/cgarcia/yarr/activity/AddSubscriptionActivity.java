/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.fragment.AddSubscriptionFragment;

/**
 * Activity that manages the addition of new subscriptions.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class AddSubscriptionActivity extends AppCompatActivity implements
        AddSubscriptionFragment.TaskCallbacks {
    // string to identify the fragment
    private static final String KEY_STRING_FRAGMENT = "add_subscription_fragment";
    private static final String KEY_STRING_NAME = "name";
    private static final String KEY_STRING_TAGS = "tags";
    private static final String KEY_BOOLEAN_PROGRESS = "progress";

    // declare the fragment
    private AddSubscriptionFragment mAddSubscriptionFragment;

    // determine whether there is loading
    private boolean mProgress;

    private String mName;
    private String mTags;

    // the view fields of the layout
    private EditText mUrlView;
    private EditText mNameView;
    private MultiAutoCompleteTextView mTagsView;
    private TextView mTagsHelpView;
    private View mProgressView;
    private View mAddSubscriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the layout
        setContentView(R.layout.activity_add_subscription);
        DefaultApplication.setupApi();

        // find the fragment
        FragmentManager fm = getFragmentManager();
        mAddSubscriptionFragment = (AddSubscriptionFragment) fm.findFragmentByTag(KEY_STRING_FRAGMENT);

        // create the fragment the first time
        if (mAddSubscriptionFragment == null) {
            // add the fragment
            mAddSubscriptionFragment = new AddSubscriptionFragment();
            fm.beginTransaction().add(mAddSubscriptionFragment, KEY_STRING_FRAGMENT).commit();
        }

        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_add_subscription);

        // set back button on toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the view fields of the layout
        mUrlView = (EditText) findViewById(R.id.activity_add_subscription_url);
        mNameView = (EditText) findViewById(R.id.activity_add_subscription_name);
        mProgressView = findViewById(R.id.activity_add_subscription_progress);
        mAddSubscriptionView = findViewById(R.id.activity_add_subscription_form);
        mTagsHelpView = (TextView) findViewById(R.id.activity_add_subscription_tags_hint);

        // get the names of the tags for auto-completion
        List<String> tagNames = new ArrayList<>(FeedDatabase.getAllTagNames());

        // set the tags field as comma-separated list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, tagNames);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_add_subscription_tags);
        mTagsView.setAdapter(adapter);
        mTagsView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mTagsView.setOnFocusChangeListener(setupHelpView());

        // handle the intent received
        try {
            handleIntent();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // set the current progress
        if (savedInstanceState == null) {
            mProgress = false;
        } else {
            mName = savedInstanceState.getString(KEY_STRING_NAME);
            mTags = savedInstanceState.getString(KEY_STRING_TAGS);
            showProgress(savedInstanceState.getBoolean(KEY_BOOLEAN_PROGRESS));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save the current state
        outState.putString(KEY_STRING_NAME, mName);
        outState.putString(KEY_STRING_TAGS, mTags);
        outState.putBoolean(KEY_BOOLEAN_PROGRESS, mProgress);
    }

    @Override
    public void onBackPressed() {
        // exit activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_subscription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button press
            case android.R.id.home:
                onBackPressed();
                break;
            // on completion press
            case R.id.menu_add_subscription_done:
                // do not call method if already called
                if (!mProgress) {
                    attemptAddSubscription();
                }
                break;
        }

        return true;
    }

    @Override
    public void onCancelAddSubscriptionUrl() {
        showProgress(false);
    }

    @Override
    public void onPostAddSubscriptionUrl(boolean success) {
        if (success) {
            if (!mName.isEmpty()) {
                // modify name of subscription if user provided one
                mAddSubscriptionFragment.addSubscriptionName(mName);
            } else if (!mTags.isEmpty()) {
                // categorize subscription if user provided any
                mAddSubscriptionFragment.addSubscriptionTags(mTags);
            } else {
                // finish activity if no other fields are given
                setResult(AddSubscriptionActivity.RESULT_OK);
                finish();
            }
        } else {
            // unknown error occurred
            showProgress(false);
            Snackbar.make(mAddSubscriptionView, R.string.error_add_subscription,
                    Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPostAddSubscriptionName() {
        if (!mTags.isEmpty()) {
            // add tags if user provided any
            mAddSubscriptionFragment.addSubscriptionTags(mTags);
        } else {
            // finish the activity if no tags given
            setResult(AddSubscriptionActivity.RESULT_OK);
            finish();
        }
    }

    @Override
    public void onPostAddSubscriptionTags() {
        // finish the activity
        setResult(AddSubscriptionActivity.RESULT_OK);
        finish();
    }

    /**
     * Returns the focus change listener that shows/hides
     * the tag hint below the tag field.
     *
     * @return the on focus change listener
     */
    private View.OnFocusChangeListener setupHelpView() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // the focus is on the tag field
                if (hasFocus) {
                    mTagsHelpView.setVisibility(View.VISIBLE);
                } else {
                    mTagsHelpView.setVisibility(View.INVISIBLE);
                }
            }
        };
    }

    /**
     * Checks to see if the intent received is shared intent or a
     * view intent. If it a shared intent, receive the plain text
     * url, else parse the xml from the view intent.
     */
    private void handleIntent() throws IOException {
        // get the calling intent
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        boolean isLoggedIn = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN,
                SharedPreferencesManager.VALUE_BOOLEAN_LOGGED_IN_DEFAULT);

        // make sure user has logged into application
        if (isLoggedIn) {
            if (Intent.ACTION_SEND.equals(action) && type != null && "text/plain".equals(type)) {
                // intent is from receiving a shared text item
                String url = intent.getStringExtra(Intent.EXTRA_TEXT);
                // set the url field if not null
                if (url != null) {
                    mUrlView.setText(url);
                    mUrlView.setSelection(mUrlView.getText().length());
                }
            } else if (Intent.ACTION_VIEW.equals(action)) {
                // intent is from opening a URL
                Uri uri = intent.getData();
                // set the url field if not null
                if (uri != null) {
                    String url = uri.toString();
                    mUrlView.setText(url);
                    mUrlView.setSelection(mUrlView.getText().length());
                }
            }
        } else {
            // start login activity if nog logged in
            Intent i = new Intent(AddSubscriptionActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    /**
     * Adds a subscription if the correct conditions are achieved.
     */
    private void attemptAddSubscription() {
        // reset error fields
        mUrlView.setError(null);
        mNameView.setError(null);
        mTagsView.setError(null);

        // get the current user-supplied values from the views
        String url = mUrlView.getText().toString();
        mName = mNameView.getText().toString();
        mTags = mTagsView.getText().toString();

        // reset focus and cancel to default
        boolean cancel = false;
        View focusView = null;

        // check for the existence of the required URL
        if (url.isEmpty()) {
            mUrlView.setError(getString(R.string.error_field_required));
            focusView = mUrlView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt to add the feed
            focusView.requestFocus();
        } else if (!DefaultApplication.isConnected(this)) {
            // display error if not connected
            Snackbar.make(mAddSubscriptionView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else {
            // attempt to add the feed
            showProgress(true);
            mAddSubscriptionFragment.addSubscriptionUrl(url);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view if true
        mAddSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
        mAddSubscriptionView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mAddSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
