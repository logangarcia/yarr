/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.Opml;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.object.Outline;
import me.cgarcia.yarr.object.Subscription;

/**
 * Fragment that imports/exports to/from OPML files.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class SettingsFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onPostImportFromOpml(boolean success);
        void onPostExportToOpml(File file);
    }

    /**
     * Calls an AsyncTask that imports subscriptions
     * from an OPML file.
     *
     * @param inputStream the input stream representing
     *                    the OPML file
     */
    public void importFromOpml(InputStream inputStream) {
        AsyncTask<Void, Void, Boolean> importFromOpmlTask = new ImportFromOpmlTask(inputStream);
        importFromOpmlTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask that exports subscriptions
     * to an OPML file.
     *
     * @param file the OPML file
     * @param name the name of the OPML file
     */
    public void exportToOpml(File file, String name) {
        AsyncTask<Void, Void, Void> exportToOpmlTask = new ExportToOpmlTask(file, name);
        exportToOpmlTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * AsyncTask that imports feeds from an OPML file.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class ImportFromOpmlTask extends AsyncTask<Void, Void, Boolean> {

        private InputStream mInputStream;
        private List<Outline> mOutlines;

        /**
         * The default constructor.
         *
         * @param inputStream the input stream representing
         *                    the OPML file
         */
        ImportFromOpmlTask(InputStream inputStream) {
            mInputStream = inputStream;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // get the outlines from the input stream
                mOutlines = new ArrayList<>(Opml.read(mInputStream));

                if (mOutlines.size() > 0) {
                    // there were outlines in the file
                    return true;
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // there were no outlines in the file
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                // parse the outlines and create subscriptions
                // from them
                for (int i = 0; i < mOutlines.size(); i++) {
                    Outline outline = mOutlines.get(i);
                    String title = outline.getTitle();
                    String link = outline.getLink();

                    AsyncTask<Void, Void, Void> addSubscriptionTask =
                            new AddSubscriptionTask(link, title);
                    addSubscriptionTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                }
            }

            mTaskCallbacks.onPostImportFromOpml(success);
        }
    }

    /**
     * AsyncTask that exports subscriptions to
     * an OPML file.
     */
    @SuppressLint("StaticFieldLeak")
    private class ExportToOpmlTask extends AsyncTask<Void, Void, Void> {

        private File mOpmlFile;
        private String mName;

        /**
         * The default constructor.
         *
         * @param opmlFile the OPML file
         * @param name     the name within the file
         */
        ExportToOpmlTask(File opmlFile, String name) {
            mOpmlFile = opmlFile;
            mName = name;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the current subscriptions to export
                List<Subscription> subscriptions =
                        FeedDatabase.getAllSubscriptions();

                // write the subscriptions to the file
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(mOpmlFile, true));
                Opml.write(mName, subscriptions, writer);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostExportToOpml(mOpmlFile);
        }
    }

    /**
     * AsyncTask that adds a new subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionTask extends AsyncTask<Void, Void, Void> {
        // declare the subscription variables
        private String mUrl;
        private String mName;

        /**
         * The default constructor.
         *
         * @param url  the URL of the subscription
         * @param name the name of the subscription
         */
        AddSubscriptionTask(String url, String name) {
            mUrl = url;
            mName = name;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // create a new subscription using a JSON object
                JSONObject jsonSubscription = new JSONObject();
                jsonSubscription.put(Feedbin.JSON_FEED_URL, mUrl);
                int status = Feedbin.createSubscription(jsonSubscription);

                if (status == Feedbin.STATUS_CREATED) {
                    // find the newly created subscription
                    JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                    for (int i = 0; i < jsonSubscriptions.length(); i++) {
                        jsonSubscription = jsonSubscriptions.getJSONObject(i);
                        if (mUrl.equals(jsonSubscription.getString(Feedbin.JSON_FEED_URL))) {
                            if (!mName.equals(jsonSubscription.getString(Feedbin.JSON_TITLE))) {
                                // update the newly created subscription with
                                // the new name
                                String subId = jsonSubscription.getString(Feedbin.JSON_ID);
                                jsonSubscription = new JSONObject();
                                jsonSubscription.put(Feedbin.JSON_TITLE, mName);
                                Feedbin.updateSubscription(subId, jsonSubscription);
                            }

                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
