/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.cgarcia.yarr.api.Feedbin;

/**
 * Fragment that adds a subscription.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class AddSubscriptionFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onCancelAddSubscriptionUrl();
        void onPostAddSubscriptionUrl(boolean success);
        void onPostAddSubscriptionName();
        void onPostAddSubscriptionTags();
    }

    /**
     * Calls an AsyncTask to add a new feed.
     *
     * @param url the URL of the feed to add
     */
    public void addSubscriptionUrl(String url) {
        AsyncTask<Void, Void, Boolean> addSubscriptionUrlTask = new AddSubscriptionUrlTask(url);
        addSubscriptionUrlTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask to change the name
     * of the recently added feed.
     *
     * @param name the new name for the feed
     */
    public void addSubscriptionName(String name) {
        AsyncTask<Void, Void, Void> addSubscriptionNameTask = new AddSubscriptionNameTask(name);
        addSubscriptionNameTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask to categorize the
     * feed based on the provided tags.
     *
     * @param tags the names of the tags
     */
    public void addSubscriptionTags(String tags) {
        List<String> tagList = Arrays.asList(tags.split(","));

        AsyncTask<Void, Void, Void> addSubscriptionTagsTask = new AddSubscriptionTagsTask(tagList);
        addSubscriptionTagsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * AsyncTask that attempts to add a subscription given a URL.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionUrlTask extends AsyncTask<Void, Void, Boolean> {

        private String mUrl;

        /**
         * The default constructor.
         *
         * @param url the URL of the feed
         */
        AddSubscriptionUrlTask(String url) {
            mUrl = url;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // create subscription
                JSONObject jsonSubscription = new JSONObject();
                jsonSubscription.put(Feedbin.JSON_FEED_URL, mUrl);
                int status = Feedbin.createSubscription(jsonSubscription);

                // check for successful creation
                if (status == Feedbin.STATUS_CREATED) {
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mTaskCallbacks.onPostAddSubscriptionUrl(success);
        }

        @Override
        protected void onCancelled() {
            mTaskCallbacks.onCancelAddSubscriptionUrl();
        }
    }

    /**
     * AsyncTask that attempts to modify the name of a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionNameTask extends AsyncTask<Void, Void, Void> {

        private String mName;

        /**
         * The default constructor.
         *
         * @param name the new name for the subscription
         */
        AddSubscriptionNameTask(String name) {
            mName = name;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the recently added subscription
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(0);
                String currentName = jsonSubscription.getString(Feedbin.JSON_TITLE);
                // modify name of recently added subscription
                // if name is different
                if (!currentName.equals(mName)) {
                    String subId = jsonSubscription.getString(Feedbin.JSON_ID);
                    JSONObject newName = new JSONObject();
                    newName.put(Feedbin.JSON_TITLE, mName);
                    Feedbin.updateSubscription(subId, newName);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostAddSubscriptionName();
        }
    }

    /**
     * AsyncTask that attempts to tag a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionTagsTask extends AsyncTask<Void, Void, Void> {

        private List<String> mTags;

        /**
         * The default constructor.
         *
         * @param tags the list of tags
         */
        AddSubscriptionTagsTask(List<String> tags) {
            mTags = new ArrayList<>(tags);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the recently added subscription
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(0);
                String feedId = jsonSubscription.getString(Feedbin.JSON_FEED_ID);
                // loop through and add the given tags
                for (int i = 0; i < mTags.size(); i++) {
                    String tagName = mTags.get(i);
                    JSONObject newTag = new JSONObject();
                    newTag.put(Feedbin.JSON_FEED_ID, feedId);
                    newTag.put(Feedbin.JSON_NAME, tagName);
                    Feedbin.createTag(newTag);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostAddSubscriptionTags();
        }
    }
}
