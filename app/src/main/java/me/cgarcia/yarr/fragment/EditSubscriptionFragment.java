/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.FeedDatabase;

/**
 * Fragment that edits a subscription.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class EditSubscriptionFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onPostEditSubscriptionName();
        void onPostEditSubscriptionTags();
    }

    /**
     * Calls an AsyncTask to edit the name of a subscription.
     *
     * @param subscriptionId the ID of the subscription to edit
     * @param name           the new name of the subscription
     */
    public void editSubscriptionName(String subscriptionId, String name) {
        AsyncTask<Void, Void, Void> editSubscriptionNameTask =
                new EditSubscriptionNameTask(subscriptionId, name);
        editSubscriptionNameTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask to edit the tags of a subscription.
     *
     * @param subscriptionId the ID of the subscription to edit
     * @param tags           the new tags of the subscription
     */
    public void editSubscriptionTags(String subscriptionId, String tags) {
        List<String> tagList = Arrays.asList(tags.split(","));

        AsyncTask<Void, Void, Void> editSubscriptionTagsTask =
                new EditSubscriptionTagsTask(subscriptionId, tagList);
        editSubscriptionTagsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * AsyncTask that attempts to modify the name of a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class EditSubscriptionNameTask extends AsyncTask<Void, Void, Void> {
        // the ID of the subscription to rename
        private String mFeedId;
        private String mNewName;

        /**
         * The default constructor.
         *
         * @param feedId the ID of the subscription to modify
         */
        EditSubscriptionNameTask(String feedId, String newName) {
            mFeedId = feedId;
            mNewName = newName;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // update subscription using a JSON object
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                for (int i = 0; i < jsonSubscriptions.length(); i++) {
                    JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(i);
                    if (mFeedId.equals(jsonSubscription.getString(Feedbin.JSON_FEED_ID))) {
                        String subId = jsonSubscription.getString(Feedbin.JSON_ID);
                        JSONObject jsonSubName = new JSONObject();
                        jsonSubName.put(Feedbin.JSON_TITLE, mNewName);
                        Feedbin.updateSubscription(subId, jsonSubName);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostEditSubscriptionName();
        }
    }

    /**
     * AsyncTask that attempts to tag a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class EditSubscriptionTagsTask extends AsyncTask<Void, Void, Void> {

        private String mFeedId;
        private List<String> mTags;

        EditSubscriptionTagsTask(String feedId, List<String> tags) {
            mFeedId = feedId;
            mTags = new ArrayList<>(tags);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the list of tags
                JSONArray jsonTags = Feedbin.getTags();
                List<String> subTagNames = FeedDatabase.getTagNamesBySubscription(mFeedId);
                boolean isAdded = true;

                // determine whether a tag has been added
                for (int i = 0; i < mTags.size(); i++) {
                    String tagName = mTags.get(i);
                    for (int v = 0; v < subTagNames.size(); v++) {
                        String subTagName = subTagNames.get(v);
                        // tag exists; do not add it
                        if (tagName.equals(subTagName)) {
                            isAdded = false;
                        }
                    }

                    // a new tag was added
                    if (isAdded) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Feedbin.JSON_FEED_ID, mFeedId);
                        jsonObject.put(Feedbin.JSON_NAME, tagName);
                        Feedbin.createTag(jsonObject);
                    }

                    // reset boolean value
                    isAdded = true;
                }

                boolean isRemoved = true;

                // determine whether a tag was removed
                for (int i = 0; i < subTagNames.size(); i++) {
                    String subTagName = subTagNames.get(i);
                    for (int v = 0; v < mTags.size(); v++) {
                        String tagName = mTags.get(v);
                        // the tag was not removed
                        if (subTagName.equals(tagName)) {
                            isRemoved = false;
                            break;
                        }
                    }

                    // the tag was removed
                    if (isRemoved) {
                        // find the tag and remove it
                        for (int v = 0; v < jsonTags.length(); v++) {
                            JSONObject jsonTag = jsonTags.getJSONObject(v);
                            if (jsonTag.getString(Feedbin.JSON_FEED_ID).equals(mFeedId)
                                    && jsonTag.getString(Feedbin.JSON_NAME).equals(subTagName)) {
                                String tagId = jsonTag.getString(Feedbin.JSON_ID);
                                Feedbin.deleteTag(tagId);
                                break;
                            }
                        }
                    }

                    // reset the boolean value
                    isRemoved = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostEditSubscriptionTags();
        }
    }
}
