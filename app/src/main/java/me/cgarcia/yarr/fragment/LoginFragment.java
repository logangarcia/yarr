/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.IOException;

import me.cgarcia.yarr.api.Feedbin;

/**
 * Fragment that authenticates user credentials.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class LoginFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onPostAuthenticate(boolean success);
    }

    /**
     * Calls an AsyncTask to authenticate user credentials.
     *
     * @param email    the user-provided email
     * @param password the user-provided password
     */
    public void authenticate(String email, String password) {
        AsyncTask<Void, Void, Boolean> authenticateTask = new AuthenticateTask(email, password);
        authenticateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * AsyncTask that offers login via email/password.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AuthenticateTask extends AsyncTask<Void, Void, Boolean> {

        private String mEmail;
        private String mPassword;

        /**
         * Thd default constructor.
         *
         * @param email    the user-provided email
         * @param password the user-provided password
         */
        AuthenticateTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                int status = Feedbin.authenticate(mEmail, mPassword);
                if (status == Feedbin.STATUS_OK) {
                    // authentication was successful
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //default to returning false if status is not OK
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mTaskCallbacks.onPostAuthenticate(success);
        }
    }
}
