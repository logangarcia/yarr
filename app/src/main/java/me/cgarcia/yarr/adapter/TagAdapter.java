/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * ExpandableRecyclerViewAdapter that manages the views and behaviors of
 * tags and tagged subscriptions in a list.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class TagAdapter extends ExpandableRecyclerViewAdapter<TagAdapter.TagViewHolder, TagAdapter.SubViewHolder> {
    // declare inflater, tag callback, and subscription callback
    private LayoutInflater mInflater;
    private ItemClickCallbacks mItemClickCallbacks;
    private Context mContext;

    /**
     * The default constructor.
     *
     * @param groups  the expandable group list
     * @param context the context of the activity
     */
    public TagAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public TagViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_tag, parent, false);
        return new TagViewHolder(view);
    }

    @Override
    public SubViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_subscription_tagged, parent, false);
        return new SubViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SubViewHolder holder, int flatPosition, ExpandableGroup group,
                                      int childIndex) {
        // get subscription at index
        final Subscription sub = (Subscription) (group.getItems().get(childIndex));

        // set the subscription icon
        Bitmap bitmap = sub.getIcon();
        holder.mSubIcon.setImageBitmap(DefaultApplication.formatBitmap(bitmap, mContext));

        // set the subscription name
        holder.mSubName.setText(sub.getName());

        // set the unread count of the subscription
        if (sub.getCount() == 0) {
            holder.mSubCount.setText("");
        } else {
            holder.mSubCount.setText(String.valueOf(sub.getCount()));
        }

        // set the selection of the subscription
        String subId = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_SUB_ID,
                SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
        holder.mChildContainer.setSelected(subId.equals(sub.getId()));
    }

    @Override
    public void onBindGroupViewHolder(TagViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        // get the tag from the group
        final Tag tag = (Tag) group;

        // set the name of the tag
        holder.mTagName.setText(group.getTitle());

        // set the unread count of the tag
        if (tag.getCount() == 0) {
            holder.mTagCount.setText("");
        } else {
            holder.mTagCount.setText(String.valueOf(tag.getCount()));
        }

        // set the selection of the tag
        String tagId = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_TAG_ID,
                SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
        holder.mParentContainer.setSelected(tagId.equals(tag.getId()));
    }

    /**
     * Interface that receives the position of a tag
     * or its children.
     */
    public interface ItemClickCallbacks {
        void onTagItemClick(int p);
        void onTaggedSubscriptionItemClick(int p);
    }

    /**
     * Sets the tag item click callback.
     *
     * @param itemClickCallback the item click callback
     */
    public void setItemClickCallbacks(final ItemClickCallbacks itemClickCallback) {
        mItemClickCallbacks = itemClickCallback;
    }

    /**
     * GroupViewHolder that receives and holds the view
     * of the tag.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    class TagViewHolder extends GroupViewHolder implements View.OnClickListener {
        // declare the layout views
        private ImageView mArrow;
        private TextView mTagName;
        private TextView mTagCount;
        private View mParentContainer;

        /**
         * The default constructor.
         *
         * @param itemView the current view
         */
        TagViewHolder(View itemView) {
            super(itemView);

            // set the layout views
            mArrow = (ImageView) itemView.findViewById(R.id.list_tag_arrow);
            mTagName = (TextView) itemView.findViewById(R.id.list_tag_name);
            mTagCount = (TextView) itemView.findViewById(R.id.list_tag_unread);
            mParentContainer = itemView.findViewById(R.id.list_tag_root);

            // set the listeners
            mTagName.setOnClickListener(this);
            mTagCount.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.list_tag_name || v.getId() == R.id.list_tag_unread) {
                // get position of tag as long as click event
                // is not expand/collapse arrow
                mItemClickCallbacks.onTagItemClick(getAdapterPosition());
            } else {
                // expand/collapse the tag
                super.onClick(v);
            }
        }

        @Override
        public void expand() {
            // flip arrow on expand
            mArrow.setImageResource(R.drawable.ic_arrow_up_24dp);
        }

        @Override
        public void collapse() {
            // flip arrow on collapse
            mArrow.setImageResource(R.drawable.ic_arrow_down_24dp);
        }
    }

    /**
     * ChildViewHolder that receives and holds the view
     * of the tagged subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    class SubViewHolder extends ChildViewHolder implements View.OnClickListener {
        // declare the layout views
        private ImageView mSubIcon;
        private TextView mSubName;
        private TextView mSubCount;
        private View mChildContainer;

        /**
         * The default constructor.
         *
         * @param itemView the current view
         */
        SubViewHolder(View itemView) {
            super(itemView);

            // set the layout views
            mSubIcon = (ImageView) itemView.findViewById(R.id.list_subscription_tagged_icon);
            mSubName = (TextView) itemView.findViewById(R.id.list_subscription_tagged_name);
            mSubCount = (TextView) itemView.findViewById(R.id.list_subscription_tagged_unread);
            mChildContainer = itemView.findViewById(R.id.list_subscription_tagged_root);

            // set the listener
            mChildContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.list_subscription_tagged_root) {
                // get position of subscription from adapter position
                mItemClickCallbacks.onTaggedSubscriptionItemClick(getAdapterPosition());
            }
        }
    }
}
